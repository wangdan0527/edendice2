app.controller('InvestCtrl', function($scope, $location, AccountsSvc, ApiSvc, AuthSvc, PageSvc) {
    PageSvc.title = 'Account - Invest';
    $scope.AccountsSvc = AccountsSvc;
    $scope.AuthSvc = AuthSvc;
    $scope.account = 'invest';
    $scope.data = {
        invest: '',
        divest: '',
        divestCommission: Math.floor(parseFloat(config.divest_commission) * 100)
    };

    $scope.invest = function(account) {
        var invest = Math.floor(parseFloat($scope.data.invest) * CONVERSION_RATIO);
        ApiSvc
            .post('/accounts/' + account + '/invest', {
                invest: invest
            })
            .success(function(data) {
                AccountsSvc.accounts = data.data.accounts;
            })
            .error(ApiSvc.makeErrorHandler($scope, 'Failed to invest funds.'));

        $scope.data.invest = '';
    };

    $scope.divest = function(account) {
        var divest = Math.floor(parseFloat($scope.data.divest) * CONVERSION_RATIO);
        ApiSvc
            .post('/accounts/' + account + '/divest', {
                divest: divest
            })
            .success(function(data) {
                AccountsSvc.accounts = data.data.accounts;
            })
            .error(ApiSvc.makeErrorHandler($scope, 'Failed to divest funds.'));

        $scope.data.divest = '';
    };
});

