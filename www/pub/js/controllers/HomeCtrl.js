app.controller('HomeCtrl', function($scope, $location, AuthSvc) {
    if (AuthSvc.isLoggedIn) {
        $location.path('/dice').replace();
    } else {
        $location.path('/login').replace();
    }
});
