app.controller('LeaderboardCtrl', function($scope, $location, $timeout, ApiSvc, AuthSvc, CurrenciesSvc, PageSvc) {
    PageSvc.title = 'Leaderboard';
    $scope.AuthSvc = AuthSvc;
    $scope.CurrenciesSvc = CurrenciesSvc;

    $scope.data = {};

    $scope.fetch = function() {
        ApiSvc
            .get('/leaderboard')
            .success(function(data) {
                $timeout(function() {
                    $scope.data = data.data;
                });
            })
            .error(ApiSvc.makeErrorHandler($scope, 'Failed to fetch leaderboards.'));
    };

    $scope.fetch();
});
