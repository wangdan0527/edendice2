app.controller('GameAccountsCtrl', function($filter, $scope, AccountsSvc) {
    $scope.AccountsSvc = AccountsSvc;

    $scope.label = function(account) {
        return account.currency_name + ' - ' + $filter('balance')(account.balance);
    };
});

