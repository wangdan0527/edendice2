app.controller('ContactCtrl', function($http, $route, $scope, ApiSvc, AuthSvc, PageSvc) {
    PageSvc.title = 'Contact';
    $scope.AuthSvc = AuthSvc;
    $scope.errors = { };

    $scope.reset = function() {
        $scope.data = {
            name: '',
            email: '',
            phone: '',
            message: '',
            captcha: ''
        };
    };

    $scope.submit = function() {
        ApiSvc
            .post('/contacts', $scope.data)
            .success(function() {
                $route.reload();
            })
            .error(ApiSvc.makeErrorHandler($scope, 'Failed to send message.'));
    };

    $scope.reset();
});
