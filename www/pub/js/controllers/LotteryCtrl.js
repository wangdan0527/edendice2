app.controller('LotteryCtrl', function($scope, $location, $timeout, AccountsSvc, ApiSvc, AuthSvc, GameSvc, PageSvc, PlaysSvc) {
    PageSvc.title = 'Lottery';
    $scope.AuthSvc = AuthSvc;
    $scope.PlaysSvc = PlaysSvc;
    $scope.game_id = 3;
    $scope.game_name = 'Lottery';
    $scope.game_instructions = '<p>TODO</p>';
    $scope.data = {
        account: null,
        bet: 0.00000001,
        numbers: []
    };
    $scope.available = [];
    $scope.numbers = {};

    $scope.random = function() {
        var numbers = [];
        while (numbers.length < config.lottery_count) {
            var number = Math.ceil(Math.random() * config.lottery_max + config.lottery_min - 1).toString();
            if (numbers.indexOf(number) == -1) {
                numbers.push(number);
            }
        }
        $scope.numbers = {};
        angular.forEach(numbers, function(v) {
            $scope.numbers[v] = true;
        });
    };

    $scope.clear = function() {
        $scope.numbers = {};
    };

    $scope.tooMany = function() {
        var count = 0;
        angular.forEach($scope.numbers, function(v) {
            if (v) {
                count ++;
            }
        });
        return count > config.lottery_count;
    };

    $scope.$watch('numbers', function(n) {
        var numbers = [];
        angular.forEach(n, function(v, k) {
            if (v) {
                numbers.push(k);
            }
        });
        $scope.data.numbers = numbers;
    });

    GameSvc.handleBetShortcuts($scope);
    GameSvc.selectAccount($scope);

    for (var i = config.lottery_min; i <= config.lottery_max; i ++) {
        $scope.available.push(i);
    }
});
