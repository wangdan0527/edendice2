app.controller('EdendiceCtrl', function($scope, $location, $timeout, AccountsSvc, ApiSvc, AuthSvc, GameSvc, PageSvc, PlaysSvc) {
    PageSvc.title = 'Eden Dice';
    $scope.AuthSvc = AuthSvc;
    $scope.PlaysSvc = PlaysSvc;
    $scope.game_id = 2;
    $scope.game_name = 'Eden Dice';
    $scope.game_instructions = '<p>TODO</p>';
    $scope.data = {
        roll: null,
        payout: null,
        bet: 0.00000001,
        account: null
    };
    $scope.payouts = {
        '4.2': '4.2x payout with a 80% refund for a roll on either side',
        '4.4': '4.4x payout with a 70% refund for a roll on either side',
        '4.6': '4.6x payout with a 60% refund for a roll on either side',
        '4.8': '4.8x payout with a 50% refund for a roll on either side'
    };
    $scope.rolls = {
        '1': '1',
        '2': '2',
        '3': '3',
        '4': '4',
        '5': '5',
        '6': '6',
        'random': 'Random'
    };

    $scope.random = function() {
        $scope.data.roll = Math.ceil(Math.random() * 6).toString();
    };

    $scope.min = function() {
        $scope.data.bet = 1;
    };

    $scope.halve = function() {
        $scope.data.bet = Math.ceil($scope.data.bet / 2);
    };

    $scope.double = function() {
        $scope.data.bet *= 2;
    };

    $scope.max = function() {
        $scope.data.bet = 100;
    };

    GameSvc.handleBetShortcuts($scope);
    GameSvc.selectAccount($scope);
});
