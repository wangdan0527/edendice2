app.controller('ChatCtrl', function($scope, $timeout, ApiSvc, AuthSvc, ExchangeSvc) {
    $scope.data = {
        messages: [],
        currentMessage: ''
    };
    $scope.ExchangeSvc = ExchangeSvc;

    $scope.send = function() {
        var message = $scope.data.currentMessage.trim();
        if (message) {
            ExchangeSvc.emit('chat', message);
        }
        $scope.data.currentMessage = '';
    };

    ExchangeSvc.on('chat', function(data) {
        $timeout(function() {
            $scope.data.messages.push(data);
            $timeout(function() {
                $('#sidebar-navigation').scrollTo($('#sidebar-navigation article:last-child'));
            });
        });
    });

    ExchangeSvc.on('chat error', function(data) {
        alert(data);
    });
});

