app.controller('PlayCtrl', function($interval, $scope, $timeout, AccountsSvc, ApiSvc) {
    $scope.data = {
        autoplay: false,
        playLabel: 'Play',
        playing: false,
        playCls: '',
        count: '',
        stopMin: '',
        stopMax: '',
        winAction: 'no-action',
        loseAction: 'starting-bet',
        increaseBetPercentage: '',
        decreaseBetPercentage: '',
        startingBet: 0
    };

    var $gameScope = $scope.$parent.$parent;

    $scope.$watch('data.autoplay', function(v) {
        if (v) {
            $('#autoplay-options-block, tr.collapsable').removeClass('hidden');
            $scope.data.autoplay = true;
            $scope.stopAutoplay();
        } else {
            $('#autoplay-options-block, tr.collapsable').addClass('hidden');
            $scope.data.autoplay = false;
            $scope.data.playLabel = 'Play';
            $scope.data.playing = false;
            $scope.data.playCls = '';
        }
    });

    $scope.play = function() {
        if ($scope.data.playing) {
            $scope.stopAutoplay();
        } else if ($scope.data.autoplay) {
            $scope.data.playLabel = 'Cancel';
            $scope.data.playing = true;
            $scope.data.playCls = 'play-cancel';
            $scope.data.startingBet = $gameScope.data.bet;
            $scope.autoplay();
        } else {
            $scope.submit();
        }
    };

    $scope.submit = function(fn) {
        var data = {};
        angular.forEach($gameScope.data, function(v, k) {
            data[k] = v;
        });
        data.game = $gameScope.game_id;
        data.bet = parseFloat(data.bet) * CONVERSION_RATIO;
        ApiSvc
            .post('/plays', data)
            .success(function(data) {
                $gameScope.errors = {};
                AccountsSvc.accounts = data.data.accounts;
                if (fn) {
                    fn(data);
                }
            })
            .error(function(data) {
                ApiSvc.makeErrorHandler($gameScope, 'Failed to submit play.')(data);
                if ($scope.data.autoplay) {
                    $scope.stopAutoplay();
                }
            });
    };

    $scope.autoplay = function() {
        if (!$scope.data.playing) {
            return;
        }
        if ($scope.data.count !== '') {
            $scope.data.count = Math.max(0, parseInt($scope.data.count, 10));
            if ($scope.data.count == 0) {
                $scope.stopAutoplay();
                return;
            }
            $scope.data.count -= 1;
        }
        $scope.submit(function(data) {
            var balance = AccountsSvc.getAccount($gameScope.data.account).balance / CONVERSION_RATIO;
            var stopMin = Math.max(0, parseFloat($scope.data.stopMin));
            var stopMax = parseFloat($scope.data.stopMax);
            if (balance <= stopMin || (stopMax > 0 && balance >= stopMax)) {
                $scope.stopAutoplay();
                return;
            }
            var percentage;
            if (data.data.result.result == 'lose') {
                if ($scope.data.loseAction == 'starting-bet') {
                    $gameScope.data.bet = $scope.data.startingBet;
                } else if ($scope.data.loseAction == 'decrease-bet') {
                    percentage = parseInt($scope.data.decreaseBetPercentage, 10);
                    if (isNaN(percentage)) {
                        percentage = 0;
                    }
                    percentage = Math.max(0, percentage);
                    $scope.data.decreaseBetPercentage = percentage;
                    $gameScope.data.bet = ($gameScope.data.bet * (1 - percentage / 100)).toFixed(8);
                }
            } else {
                if ($scope.data.winAction == 'no-action') {
                    // do nothing
                } else if ($scope.data.winAction == 'increase-bet') {
                    percentage = parseInt($scope.data.increaseBetPercentage, 10);
                    if (isNaN(percentage)) {
                        percentage = 0;
                    }
                    percentage = Math.max(0, percentage);
                    $scope.data.increaseBetPercentage = percentage;
                    $gameScope.data.bet = ($gameScope.data.bet * (1 + percentage / 100)).toFixed(8);
                }
            }
            $timeout($scope.autoplay, 1000);
        });
    };

    $scope.stopAutoplay = function() {
        $scope.data.playLabel = 'Autoplay';
        $scope.data.playing = false;
        $scope.data.playCls = '';
    };

    $scope.$watch('data.winAction', function(value) {
        if (value == 'increase-bet') {
            $('#increase-percentage').focus();
        }
    });

    $scope.$watch('data.loseAction', function(value) {
        if (value == 'decrease-bet') {
            $('#decrease-percentage').focus();
        }
    });
});