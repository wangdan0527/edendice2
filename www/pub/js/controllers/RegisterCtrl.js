app.controller('RegisterCtrl', function($location, $scope, AffiliateSvc, ApiSvc, AuthSvc, PageSvc) {
    PageSvc.title = 'Register';
    $scope.data = {
        username: '',
        password: '',
        password_confirm: '',
        affiliate_code: AffiliateSvc.affiliate_code
    };
    $scope.errors = { };

    $scope.submit = function() {
        ApiSvc
            .post('/users', $scope.data)
            .success(function() {
                ApiSvc
                    .post('/sessions', $scope.data)
                    .success(function(data) {
                        AuthSvc.login(data.data);
                        $location.path('/home');
                    })
                    .error(ApiSvc.makeErrorHandler($scope, 'Failed to submit registration.'));
            })
            .error(ApiSvc.makeErrorHandler($scope, 'Failed to submit registration.'));
    };
});
