app.controller('LoginCtrl', function($http, $location, $scope, ApiSvc, AuthSvc, PageSvc) {
    PageSvc.title = 'Login';
    $scope.data = {
        username: '',
        password: '',
        mfa: ''
    };
    $scope.errors = { };

    $scope.submit = function() {
        ApiSvc
            .post('/sessions', $scope.data)
            .success(function(data) {
                AuthSvc.login(data.data);
                $location.path('/home');
            })
            .error(ApiSvc.makeErrorHandler($scope, 'Failed to log in.'));
    };

    if (AuthSvc.data) {
        $location.path('/');
    }
});
