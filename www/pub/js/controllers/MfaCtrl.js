app.controller('MfaCtrl', function($scope, $timeout, ApiSvc, AuthSvc) {
    $scope.data = {
        enabled: '0'
    };

    ApiSvc
        .get('/users/' + AuthSvc.data.user_id + '/mfa')
        .success(function(data) {
            $scope.data.enabled = data.data.enabled ? '1' : '0';
            $scope.data.img = data.data.img;
            $scope.hook();
        })
        .error(ApiSvc.makeErrorHandler($scope, 'Failed to get current MFA setting.'));

    $scope.hook = function() {
        $scope.$watch('data.enabled', function(newValue, oldValue) {
            if (newValue != oldValue) {
                $scope.data.img = '';
                ApiSvc
                    .post('/users/' + AuthSvc.data.user_id + '/mfa', {
                        enabled: newValue == '1'
                    })
                    .success(function (data) {
                        $scope.data.img = data.data.img;
                    })
                    .error(ApiSvc.makeErrorHandler($scope, 'Failed to update MFA setting.'));
            }
        });
    };
});