app.controller('StatsCtrl', function($scope, $timeout, ExchangeSvc) {
    $scope.data = {};

    ExchangeSvc.on('stats', function(data) {
        $timeout(function() {
            $scope.data = data;
        });
    });
});
