app.controller('SidebarCtrl', function($scope, AccountsSvc, ApiSvc, AuthSvc, ExchangeSvc) {
    $scope.isOpen = false;
    $scope.AccountsSvc = AccountsSvc;
    $scope.data = {
        username: '',
        chatCount: 0
    };

    $scope.$on('sidebar.toggle', function() {
        $scope.toggle();
    });

    $scope.$on('logout', function() {
        $scope.close();
    });

    $scope.toggle = function() {
        if ($scope.isOpen) {
            $scope.close()
        } else {
            $scope.open();
        }
    };

    var sidebar = $("#sidebar"),
        mainSection = $("#has-sidebar, #header"),
        twitterIcon = $("a#twitter");

    $scope.open = function() {
        if ($scope.isOpen) {
            return;
        }
        $scope.isOpen = true;
        sidebar.removeClass("collapsed");

        twitterIcon.animate({
            "margin-right": 10
        }, 300);

        if($(window).width() > 980) {
            mainSection.animate({
                "margin-left": 260
            }, 300)
        } else {
            mainSection.animate({
                "left": 260
            }, 300)
        }
    };

    $scope.close = function() {
        if (!$scope.isOpen) {
            return;
        }
        $scope.isOpen = false;
        mainSection.stop();
        twitterIcon.stop();

        twitterIcon.animate({
            "margin-right": 60
        }, 300);

        if($(window).width() > 980) {
            mainSection.animate({
                    "margin-left": 0
                }, 300,
                function() {
                    sidebar.addClass("collapsed");
                });
        } else {
            mainSection.animate({
                    "left": 0
                }, 300,
                function() {
                    sidebar.addClass("collapsed");
                });
        }
    };

    ExchangeSvc.on('chat', function() {
        if (!$scope.isChatOpen()) {
            $scope.data.chatCount += 1;
        }
    });

    $scope.isChatOpen = function() {
        return $('#chats').hasClass('active');
    }
});
