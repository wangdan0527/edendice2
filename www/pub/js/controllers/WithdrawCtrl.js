app.controller('WithdrawCtrl', function($route, $scope, AccountsSvc, ApiSvc) {
    $scope.data = {};
    $scope.errors = {};

    $scope.submit = function(account) {
        ApiSvc
            .post(
                '/accounts/' + account + '/withdrawals',
                {
                    amount: Math.floor(parseFloat($scope.data.amount) * CONVERSION_RATIO),
                    address: $scope.data.address
                }
            )
            .success(function(data) {
                AccountsSvc.accounts = data.data.accounts;
            })
            .error(ApiSvc.makeErrorHandler(
                $scope,
                'Failed to submit request.'
            ));
    };
});

