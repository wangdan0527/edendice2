app.controller('DepositsCtrl', function($location, $scope, AccountsSvc, ApiSvc, AuthSvc, PageSvc) {
    PageSvc.title = 'Account - Deposits';
    $scope.AccountsSvc = AccountsSvc;
    $scope.AuthSvc = AuthSvc;
    $scope.account = 'deposits';

    $scope.newAddress = function(account) {
        ApiSvc
            .post('/accounts/' + account + '/addresses', {})
            .success(function(data) {
                angular.forEach(AccountsSvc.accounts, function(v, k) {
                    if (v.id == account) {
                        AccountsSvc.accounts[k].address = data.data.address;
                    }
                });
            })
            .error(ApiSvc.makeErrorHandler(
                $scope,
                'Failed to get new address.'
            ));
    };
});
