app.controller('AffiliateCtrl', function($scope, $location, ApiSvc, AuthSvc, PageSvc) {
    PageSvc.title = 'Account - Affiliate';
    $scope.AuthSvc = AuthSvc;
    $scope.account = 'affiliate';

    $scope.fetch = function() {
        ApiSvc
            .get('/users/' + AuthSvc.data.user_id + '/affiliate')
            .success(function(data) {
                $scope.data = data.data;
            })
            .error(ApiSvc.makeErrorHandler($scope, 'Failed to fetch affiliate details.'));
    };

    $scope.fetch();
});

