app.controller('SettingsCtrl', function($scope, $location, AccountsSvc, ApiSvc, AuthSvc, PageSvc) {
    PageSvc.title = 'Account - Settings';
    $scope.AccountsSvc = AccountsSvc;
    $scope.AuthSvc = AuthSvc;
    $scope.account = 'settings';
    $scope.errors = { };
    $scope.data = {
        addresses: {}
    };

    $scope.changeSeed = function() {
        ApiSvc
            .post(
                '/users/' + AuthSvc.data.user_id + '/keypairs',
                { user_key: $scope.data.user_key }
            )
            .success(function(data) {
                $scope.data.user_key_hash = data.data.user_key_hash;
            })
            .error(ApiSvc.makeErrorHandler($scope, 'Failed to change seed.'));
    };

    $scope.fetchKeypair = function() {
        ApiSvc
            .get('/users/' + AuthSvc.data.user_id + '/keypair')
            .success(function(data) {
                $scope.data.user_key_hash = data.data.user_key_hash;
            })
            .error(ApiSvc.makeErrorHandler($scope, 'Failed to fetch hash.'));
    };

    $scope.fetchKeypair();

    $scope.changePassword = function() {
        ApiSvc
            .post('/users/' + AuthSvc.data.user_id + '/password', {
                current_password: $scope.data.current_password,
                new_password: $scope.data.new_password,
                new_password_confirm: $scope.data.new_password_confirm
            })
            .success(function(data) {
                alert('Password changed.');
                $scope.data.current_password = '';
                $scope.data.new_password = '';
                $scope.data.new_password_confirm = '';
            })
            .error(ApiSvc.makeErrorHandler(
                $scope,
                'Failed to change password.'
            ));
    };

    $scope.updateAddress = function(account) {
        ApiSvc
            .post('/accounts/' + account + '/emergency-address', {
                address: $scope.data.addresses[account]
            })
            .error(ApiSvc.makeErrorHandler($scope, 'Failed to update address.'));
    };

    $scope.$on('accounts', function(e, accounts) {
        angular.forEach(accounts, function(account) {
            $scope.data.addresses[account.id] = account.emergency_address;
        });
    });

    if (AccountsSvc.accounts.length > 0) {
        angular.forEach(AccountsSvc.accounts, function(account) {
            $scope.data.addresses[account.id] = account.emergency_address;
        });
    }
});
