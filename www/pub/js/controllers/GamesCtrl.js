app.controller('GamesCtrl', function($rootScope, $scope, CurrenciesSvc) {
    $scope.CurrenciesSvc = CurrenciesSvc;

    $scope.toggleSidebar = function() {
        $rootScope.$broadcast('sidebar.toggle');
    };

    $scope.label = function(currency) {
        return currency.name.toLowerCase();
    }
});

