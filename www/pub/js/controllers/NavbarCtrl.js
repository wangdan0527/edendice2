app.controller('NavbarCtrl', function($location, $scope, AuthSvc) {
    $scope.AuthSvc = AuthSvc;

    $scope.logout = function() {
        AuthSvc.logout();
        $location.path('/login');
    };
});

