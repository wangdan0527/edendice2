app.controller('PageCtrl', function($location, $rootScope, $scope, AffiliateSvc, AuthSvc, FlashSvc, PageSvc) {
    $scope.FlashSvc = FlashSvc;
    $scope.PageSvc = PageSvc;

    $(document).on('keydown', function(e) {
        if (/textarea|select/i.test(e.target.tagName) || e.target.type == 'text') {
            return;
        }
        if (e.keyCode == 73) { // i
            $rootScope.$apply(function() {
                $location.path('/deposits');
            });
        } else if (e.keyCode == 79) { // o
            $rootScope.$apply(function() {
                $location.path('/withdrawals');
            });
        } else if (e.keyCode == 80) { // p
            $rootScope.$apply(function() {
                $location.path('/settings');
            });
        } else {
            $rootScope.$broadcast('shortcut', e);
        }
    })
});
