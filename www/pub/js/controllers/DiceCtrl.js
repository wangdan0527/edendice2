app.controller('DiceCtrl', function($scope, $interval, $location, $timeout, AccountsSvc, ApiSvc, AuthSvc, GameSvc, PageSvc, PlaysSvc) {
    PageSvc.title = 'Dice';
    $scope.AuthSvc = AuthSvc;
    $scope.PlaysSvc = PlaysSvc;
    $scope.game_id = 1;
    $scope.game_name = 'Dice';
    $scope.game_instructions = '<p>TODO</p>';
    $scope.data = {
        chances: 50,
        multiplier: 2,
        payout: '0.00000001',
        bet: '0.00000001',
        side: 'high',
        house_margin: config.dice_margin,
        account: null
    };

    $scope.changeBet = function() {
        var v = parseFloat($scope.data.multiplier) * parseFloat($scope.data.bet);
        $scope.data.payout = isNaN(v) ? (0).toFixed(8) : v.toFixed(8);
    };

    $scope.changeChances = function() {
        $scope.data.multiplier = ((1 - $scope.data.house_margin) * 100 / parseFloat($scope.data.chances)).toFixed(2);
        $scope.data.payout = (parseFloat($scope.data.multiplier) * parseFloat($scope.data.bet)).toFixed(8);
    };

    $scope.$watch('data.bet', $scope.changeBet);
    $scope.$watch('data.chances', $scope.changeChances);

    $scope.$on('shortcut', function(b, e) {
        $timeout(function() {
            if (e.keyCode == 72) { // h
                $scope.data.side = 'high';
            } else if (e.keyCode == 76) { // l
                $scope.data.side = 'low';
            } else if (e.keyCode == 65) { // a
                $scope.data.chances = 5;
            } else if (e.keyCode == 70) { // f
                $scope.data.chances = 95;
            } else if (e.keyCode == 83) { // s
                $scope.data.chances -= 1;
                $scope.data.chances = Math.max(5, $scope.data.chances);
            } else if (e.keyCode == 68) { // d
                $scope.data.chances += 1;
                $scope.data.chances = Math.min(95, $scope.data.chances);
            }
        });
    });

    GameSvc.handleBetShortcuts($scope);
    GameSvc.selectAccount($scope);
});
