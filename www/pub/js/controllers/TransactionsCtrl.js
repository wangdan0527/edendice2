app.controller('TransactionsCtrl', function($scope, $location, ApiSvc, AuthSvc, PageSvc) {
    PageSvc.title = 'Account - Transactions';
    $scope.AuthSvc = AuthSvc;
    $scope.account = 'transactions';
    $scope.data = {
        types: {
            bet: 'Bet',
            deposit: 'Deposit',
            win: 'Win',
            withdrawal: 'Withdrawal',
            investment: 'Investment',
            divestment: 'Divestment',
            affiliate: 'Affiliate payout'
        },
        type: '',
        transactions: []
    };

    $scope.fetchTransactions = function(type) {
        ApiSvc
            .get('/users/' + AuthSvc.data.user_id + '/transactions?type=' + type)
            .success(function(data) {
                $scope.data.transactions = data.data;
            })
            .error(ApiSvc.makeErrorHandler(
                $scope,
                'Failed to fetch transactions.'
            ));
    };

    $scope.$watch('data.type', function() {
        $scope.fetchTransactions($scope.data.type);
    });
});

