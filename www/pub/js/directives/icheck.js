app.directive('icheck', function($parse, $timeout) {
    return {
        link: function(scope, element, attrs) {
            var ngModel = $parse(attrs.ngModel);
            return $timeout(function() {
                scope.$watch(attrs.ngModel, function() {
                    element.iCheck('update');
                });
                element.iCheck({
                    checkboxClass: 'icheckbox',
                    radioClass: 'iradio'
                });
                if (ngModel) {
                    element.on('ifChanged', function(event) {
                        if (attrs.type === 'checkbox') {
                            scope.$apply(function() {
                                return ngModel.assign(scope, event.target.checked);
                            });
                        }
                        if (attrs.type === 'radio') {
                            scope.$apply(function() {
                                return ngModel.assign(scope, event.target.value);
                            });
                        }
                    });
                }
            });
        }
    };
});

