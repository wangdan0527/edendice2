app.directive('fancybox', function($parse) {
    return {
        link: function(scope, element, attrs) {
            var ngModel = $parse(attrs.ngModel);
            element.click(function(e) {
                e.preventDefault();
                $.fancybox.open({
                    content: ngModel(scope)
                });
            });
        }
    };
});
