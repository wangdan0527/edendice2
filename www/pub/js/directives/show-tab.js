app.directive('showTab', function() {
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.click(function(e) {
                e.preventDefault();
                element.tab('show');
            });
        }
    };
});

