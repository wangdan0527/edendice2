app.directive('slider', function($parse, $timeout) {
    return {
        link: function(scope, element, attrs) {
            var ngModel = $parse(attrs.ngModel);
            $timeout(function() {
                element.slider({
                    min: parseFloat(attrs.sliderMin),
                    max: parseFloat(attrs.sliderMax),
                    step: parseFloat(attrs.sliderStep)
                });

                if (ngModel) {
                    scope.$watch(attrs.ngModel, function(newValue) {
                        element.slider('setValue', parseInt(newValue, 10));
                    });
                    element.on('slide', function(event) {
                        scope.$apply(function() {
                            ngModel.assign(scope, event.value);
                        });
                    });
                }
            });
        }
    };
});

