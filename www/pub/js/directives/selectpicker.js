app.directive('selectpicker', function($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            $timeout(function() {
                element.selectpicker();
            });

            function refresh() {
                $timeout(function() {
                    element.selectpicker('refresh');
                });
            }

            scope.$watch(attrs.ngModel, refresh, true);

            // Need to parse the options attr in order to $watch the options
            var re = new RegExp('in ([^ ]+)');
            var matches = attrs.ngOptions.match(re);
            if (matches) {
                scope.$watch(matches[1], refresh, true);
            }
        }
    };
});

