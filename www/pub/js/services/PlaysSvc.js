app.service('PlaysSvc', function(AuthSvc, ExchangeSvc) {
    this.allPlays = [];
    this.topPlays = [];
    this.myPlays = [];

    var maxLength = 25;
    var instance = this;

    ExchangeSvc.on('play', function(data) {
        if (data.bet > 0) {
            instance.allPlays.unshift(data);
            if (instance.allPlays.length > maxLength) {
                instance.allPlays.length = maxLength;
            }
        }
        if (data.username == AuthSvc.data.username) {
            instance.myPlays.unshift(data);
            if (instance.myPlays.length > maxLength) {
                instance.myPlays.length = maxLength;
            }
        }
    });
});

