app.service('ExchangeSvc', function($interval, $rootScope, AuthSvc) {
    this.socket = io();

    var instance = this;

    this.emit = function(type, data) {
        this.socket.emit(type, data);
    };

    this.on = function(type, fn) {
        this.socket.on(type, fn);
    };

    this.on('reconnect', function() {
        if (AuthSvc.isLoggedIn) {
            instance.emit('login', AuthSvc.data.token);
        }
    });

    $rootScope.$on('login', function(e, data) {
        instance.emit('login', data.token);
    });

    $rootScope.$on('logout', function() {
        instance.emit('logout');
    })
});

