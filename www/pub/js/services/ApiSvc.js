app.service('ApiSvc', function($http, $location, $rootScope, AuthSvc) {
    this.buildUrl = function(path) {
        path = path.replace(/^\//, '');
        return '/api/' + path;
    };

    this.buildHeaders = function() {
        var headers = {};
        if (AuthSvc.isLoggedIn) {
            headers['X-EdenDice-Session'] = AuthSvc.data.token;
        }
        return headers;
    };

    this.get = function(path, data) {
        var url = this.buildUrl(path);
        var headers = this.buildHeaders();
        var req = {
            method: 'GET',
            url: url,
            headers: headers,
            data: data
        };
        return $http(req);
    };

    this.post = function(path, data) {
        var url = this.buildUrl(path);
        var headers = this.buildHeaders();
        var req = {
            method: 'POST',
            url: url,
            headers: headers,
            data: data
        };
        return $http(req);
    };

    this.makeErrorHandler = function($scope, message) {
        return function(data) {
            if (data && data.code) {
                switch (data.code) {
                    case 400:
                        if ($scope) {
                            $scope.errors = data.data;
                        }
                        break;
                    case 401:
                        AuthSvc.logout();
                        $location.path('/');
                        break;
                    default:
                        alert(message);
                        break;
                }
            } else {
                alert(message);
            }
        };
    };
});
