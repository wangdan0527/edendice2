app.service('AuthSvc', function($cookies, $location, $rootScope, $timeout) {
    var cookieSession = $cookies.get('session');
    if (cookieSession) {
        this.data = JSON.parse(cookieSession);
    } else {
        this.data = null;
    }
    this.isLoggedIn = !!this.data;

    if (this.isLoggedIn) {
        var instance = this;
        $timeout(function() {
            $rootScope.$broadcast('login', instance.data);
        }, 200);
    }

    this.login = function(data) {
        this.data = data;
        this.isLoggedIn = true;
        $cookies.put('session', JSON.stringify(data));
        $rootScope.$broadcast('login', data);
    };

    this.logout = function() {
        this.data = null;
        this.isLoggedIn = false;
        $cookies.remove('session');
        $rootScope.$broadcast('logout');
    };
});

