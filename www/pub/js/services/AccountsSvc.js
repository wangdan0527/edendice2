app.service('AccountsSvc', function($rootScope, $timeout, ApiSvc, AuthSvc) {
    var instance = this;

    this.accounts = [];

    this.refresh = function() {
        if (!AuthSvc.data) {
            this.accounts = [];
            return;
        }

        ApiSvc
            .get('/users/' + AuthSvc.data.user_id + '/accounts')
            .success(function(data) {
                instance.accounts = data.data;
                $rootScope.$broadcast('accounts', data.data);
            })
            .error(ApiSvc.makeErrorHandler(
                null,
                'Failed to fetch accounts.'
            ));
    };

    this.getAccount = function(id) {
        var account = null;
        angular.forEach(this.accounts, function(v) {
            if (parseInt(v.id, 10) === parseInt(id, 10)) {
                account = v;
            }
        });
        return account;
    };

    $rootScope.$on('login', function() {
        instance.refresh();
    });
});
