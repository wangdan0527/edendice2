app.service('CurrenciesSvc', function($timeout, ApiSvc, ExchangeSvc) {
    this.currencies = {};

    var instance = this;

    ExchangeSvc.on('currency', function(data) {
        $timeout(function() {
            instance.currencies[data.name] = data;
        });
    });

    ApiSvc
        .get('/currencies')
        .success(function(data) {
            angular.forEach(data.data, function(v) {
                instance.currencies[v.name] = v;
            });
        })
        .error(ApiSvc.makeErrorHandler(null, 'Failed to fetch currencies.'));
});

