app.service('GameSvc', function($timeout, AccountsSvc) {
    this.handleBetShortcuts = function($scope) {
        $scope.$on('shortcut', function(b, e) {
            $timeout(function() {
                if (e.keyCode == 90) { // z
                    $scope.data.bet = (1 / CONVERSION_RATIO).toFixed(8);
                } else if (e.keyCode == 88) { // x
                    $scope.data.bet = Math.max(1 / CONVERSION_RATIO, $scope.data.bet / 2).toFixed(8);
                } else if (e.keyCode == 67) { // c
                    $scope.data.bet = Math.max(1 / CONVERSION_RATIO, $scope.data.bet * 2).toFixed(8);
                } else if (e.keyCode == 86) { // v
                    $scope.data.bet = 1.00000000;
                }
                $scope.data.bet = Math.min($scope.data.bet, 1.0).toFixed(8);
            });
        });
    };

    this.selectAccount = function($scope) {
        $scope.$on('accounts', function(e, accounts) {
            if (!$scope.data.account) {
                $scope.data.account = accounts[0].id;
            }
        });

        if (AccountsSvc.accounts.length > 0) {
            $scope.data.account = AccountsSvc.accounts[0].id;
        }
    }
});

