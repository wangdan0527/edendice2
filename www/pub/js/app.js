var app = angular.module('app', ['ngRoute', 'ngCookies', 'reCAPTCHA']);

var CONVERSION_RATIO = 100000000;

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/home', {
            templateUrl: 'tpl/home.html',
            controller: 'HomeCtrl'
        })
        .when('/login', {
            templateUrl: 'tpl/login.html',
            controller: 'LoginCtrl'
        })
        .when('/register', {
            templateUrl: 'tpl/register.html',
            controller: 'RegisterCtrl'
        })
        .when('/contact', {
            templateUrl: 'tpl/contact.html',
            controller: 'ContactCtrl'
        })
        .when('/dice', {
            templateUrl: 'tpl/dice.html',
            controller: 'DiceCtrl'
        })
        .when('/edendice', {
            templateUrl: 'tpl/edendice.html',
            controller: 'EdendiceCtrl'
        })
        .when('/lottery', {
            templateUrl: 'tpl/lottery.html',
            controller: 'LotteryCtrl'
        })
        .when('/leaderboard', {
            templateUrl: 'tpl/leaderboard.html',
            controller: 'LeaderboardCtrl'
        })
        .when('/provably-fair', {
            templateUrl: 'tpl/provably-fair.html',
            controller: 'ProvablyFairCtrl'
        })
        .when('/faq', {
            templateUrl: 'tpl/faq.html',
            controller: 'FaqCtrl'
        })
        .when('/transactions', {
            templateUrl: 'tpl/transactions.html',
            controller: 'TransactionsCtrl'
        })
        .when('/deposits', {
            templateUrl: 'tpl/deposits.html',
            controller: 'DepositsCtrl'
        })
        .when('/withdrawals', {
            templateUrl: 'tpl/withdrawals.html',
            controller: 'WithdrawalsCtrl'
        })
        .when('/invest', {
            templateUrl: 'tpl/invest.html',
            controller: 'InvestCtrl'
        })
        .when('/settings', {
            templateUrl: 'tpl/settings.html',
            controller: 'SettingsCtrl'
        })
        .when('/affiliate', {
            templateUrl: 'tpl/affiliate.html',
            controller: 'AffiliateCtrl'
        })
        .otherwise({
            redirectTo: '/home'
        });
}]);

app.config(['$locationProvider', function($locationProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');
}]);

app.config(function(reCAPTCHAProvider) {
    reCAPTCHAProvider.setPublicKey('6LdVp_wSAAAAAHoOGuPZiAGmGYWuEZZOgfaYwfA-');
    reCAPTCHAProvider.setOptions({ theme: 'blackglass' });
});
