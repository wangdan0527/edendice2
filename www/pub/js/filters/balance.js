app.filter('balance', function() {
    return function(input) {
        if (isNaN(input)) {
            return '0.00000000';
        }
        return (input / CONVERSION_RATIO).toFixed(8);
    }
});
