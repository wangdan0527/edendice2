app.filter('enum', function() {
    return function(input) {
        if (input == '') {
            return ''
        }
        return input[0].toUpperCase() + input.substring(1);
    }
});
