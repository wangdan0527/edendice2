app.filter('timestamp', function() {
    return function(input) {
        var date = new Date(input * 1000);
        return date.toLocaleString();
    }
});

