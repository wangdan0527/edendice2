var sidebar = $("#sidebar"),
	mainSection = $("#main-section, #header"),
	twitterIcon = $("a#twitter");

/*-------------------------------------------------------------------
 * Navigation slide-toggle
 *-------------------------------------------------------------------
*/
$(".close-panel a, #close a, #main-section-header .navbar .navbar-toggle").on("click", function() {
	if(sidebar.hasClass("collapsed")) {
		sidebar.removeClass("collapsed");

		twitterIcon.animate({
			"margin-right": 10
		}, 300)

		if($(window).width() > 980) {
			mainSection.animate({
				"margin-left": 260
			}, 300)
		} else {
			mainSection.animate({
				"left": 260
			}, 300)
		}
	} else {
		mainSection.stop();
		twitterIcon.stop();

		twitterIcon.animate({
			"margin-right": 60
		}, 300)

		if($(window).width() > 980) {
			mainSection.animate({
				"margin-left": 0
			}, 300, 
			function() {
				sidebar.addClass("collapsed");
			});
		} else {
			mainSection.animate({
				"left": 0
			}, 300, 
			function() {
				sidebar.addClass("collapsed");
			});
		}
	}
});

/*-------------------------------------------------------------------
 * Custom Checkboxes
 *-------------------------------------------------------------------
*/
$('input').iCheck({
    checkboxClass: 'icheckbox',
    radioClass: 'iradio'
});

/*-------------------------------------------------------------------
 * Boxed input numbers
 *-------------------------------------------------------------------
*/
var boxedCheckbox = $('.input-number-blocks input[type="checkbox"]');
var boxedRadioBtn = $('.input-number-blocks input[type="radio"]');

// When lottry input checkbox is checked
boxedCheckbox.on('ifChecked', function() {
	$(this).parent('.icheckbox').siblings('.control-label').addClass('active');
});

// When lottery input checkbox is unchecked
boxedCheckbox.on('ifUnchecked', function() {
	$(this).parent('.icheckbox').siblings('.control-label').removeClass('active');
});

// When lottry input checkbox is checked
boxedRadioBtn.on('ifChecked', function() {
	$(this).parent('.iradio').siblings('.control-label').addClass('active');
});

// When lottery input checkbox is unchecked
boxedRadioBtn.on('ifUnchecked', function() {
	$(this).parents('.iradio').siblings('.control-label').removeClass('active');
});

/*-------------------------------------------------------------------
 * Autoplay toggle
 *-------------------------------------------------------------------
*/
$('#autoplay').on('ifChecked', function() {
	$('#autoplay-options-block, tr.collapsable').removeClass('hidden');
});

$('#autoplay').on('ifUnchecked', function() {
	$('#autoplay-options-block, tr.collapsable').addClass('hidden');
});

/*-------------------------------------------------------------------
 * Input range slider
 *-------------------------------------------------------------------
*/
if($('.input-slide').length > 0)
	$('.input-slide').slider();

/*-------------------------------------------------------------------
 * Input number spinner helper
 *-------------------------------------------------------------------
*/
$.widget( "ui.pcntspinner", $.ui.spinner, {
    _format: function(value) {
    	return value + '%';
    },
    
    _parse: function(value) {
    	return parseFloat(value);
    }
});

/*-------------------------------------------------------------------
 * Input number spinner
 *-------------------------------------------------------------------
*/
if($( '.spinner').length > 0)
	$( '.spinner').spinner({
		min: 0
	});

if($('.percentage-spinner').length > 0)
	$('.percentage-spinner').pcntspinner({ 
	    min: 0,
	    max: 100,
	    step: 10 }
	 );

// Prevent the dropdown menu from disappearing when clicking on inputs elements 
$('#win-lose-actions .dropdown-menu').on('click', function(){
	return false;
});

/*-------------------------------------------------------------------
 * Input number spinner
 *-------------------------------------------------------------------
*/
(function blink(element, duration, speed) {
    if (duration > 0 || duration < 0) {
        if ($(element).hasClass("blink")) 
            $(element).removeClass("blink");
        else
            $(element).addClass("blink");
    }

    clearTimeout(function () {
        blink(element, duration, speed);
    });

    if (duration > 0 || duration < 0) {
        setTimeout(function () {
            blink(element, duration, speed);
        }, speed);
        duration -= 0.5;
    }
})('.bet-status', ~0, 200);