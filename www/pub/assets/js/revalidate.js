$(document).ready(function(e) {
	var formSubmit = 'no';
	var captchaValid = 'false';
	var validationField = $('form[data-validate="true"] [data-nonempty="true"]:enabled')
	// Validation method
	function validate(formId) {
		formSubmit = 'no';
		var form = $(formId),
		formValidationFields = form.find('[data-nonempty="true"]:enabled')
		
		// Add a (invalid) class to inputs that are empty or remove for those that are valid
		formValidationFields.each(function() {
			revalidate($(this))
		})
		
		// If all passes prepare to submit the form
		if(!formValidationFields.hasClass('invalid'))
			formSubmit = 'yes'
	}
	
	// Check if an input is valid or not
	function revalidate(input) {
		if(input.val() === '' || (input.attr('type') === 'email' && !isEmailAddress(input.val()))) {
			input.addClass('invalid').trigger('failure')
			
		} else {
			input.removeClass('invalid').trigger('success')
		}
		
		// Check if Captcha valid
		if(input.attr('id') === 'captcha') {
			validateCaptcha(input.val())
			
			if(captchaValid === 'false')
				input.addClass('invalid')
		}
	}
	
	// Check if email address is valid format
	function isEmailAddress(email) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i)
		return pattern.test(email)
	}
	
	// Validate Captcha
	function validateCaptcha(code) {
		$.ajax({
			type: 'POST',
			async: false,
			url: 'assets/captcha/validate.php',
			data: {
				captcha: code
			}
		}).done(function(status) {
			captchaValid = status
		}).fail(function(status) {
			console.log('Ajax request failed. File may be unavailable!')
		})
	}
	
	// Validate on form submit
	$('form[data-validate="true"]').on('submit', function() {
		validate($(this))
		
		if(formSubmit === 'yes') {
			return true;
		}
		
		return false;
	})
	
	// Revalidate on input blur
	validationField.on('blur change', function() {
		revalidate($(this))
	})


	validationField.on('success', function() {
		$(this).siblings('i').remove()
	})

	validationField.on('failure', function() {
		var errorMsg = $(this).attr('data-nonempty-message')

		if(errorMsg) {
			$(this).siblings('i').remove()
			$(this).parents('.form-group').append('<i>' + errorMsg + '</i>')
		}
	})
})