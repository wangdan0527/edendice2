module.exports = function(grunt) {
    grunt.initConfig({
        concat: {
            dist: {
                src: [
                    'pub/assets/js/jquery.js',
                    'pub/assets/js/jquery-ui.js',
                    'pub/assets/js/jquery-ui.js',
                    'pub/assets/js/icheck.js',
                    'pub/assets/js/input-slider.js',
                    'pub/assets/js/input-spinner.js',
                    'pub/assets/js/select.js',
                    'pub/assets/js/bootstrap.js',
                    'pub/bower_components/angular/angular.min.js',
                    'pub/bower_components/angular-route/angular-route.min.js',
                    'pub/bower_components/angular-cookies/angular-cookies.min.js',
                    'pub/bower_components/fancybox/source/jquery.fancybox.js',
                    'pub/bower_components/jquery.scrollTo/jquery.scrollTo.min.js',
                    'pub/bower_components/angular-re-captcha/angular-re-captcha.js',
                    'pub/js/app.js',
                    'pub/js/controllers/*.js',
                    'pub/js/directives/*.js',
                    'pub/js/filters/*.js',
                    'pub/js/services/*.js'
                ],
                dest: 'pub/js/compiled.js'
            }
        },
        cssmin: {
            options: {
                rebase: true
            },
            target: {
                files: {
                    'pub/css/compiled.css': [
                        'pub/assets/css/bootstrap.css',
                        'pub/assets/css/icheck.css',
                        'pub/assets/css/input-slider.css',
                        'pub/assets/css/input-spinner.css',
                        'pub/assets/css/jquery-ui.css',
                        'pub/assets/css/select.css',
                        'pub/assets/css/styles.css',
                        'pub/bower_components/fancybox/source/jquery.fancybox.css',
                        'pub/css/app.css'
                    ]
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default', ['concat', 'cssmin']);
};
