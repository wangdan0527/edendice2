<?php

require_once __DIR__ . '/../bootstrap.php';

function h($string) {
    return htmlspecialchars($string, ENT_QUOTES, 'utf-8');
}

function u($string) {
    return urlencode($string);
}
