<?php include __DIR__ . '/header.php' ?>

<h1>Users</h1>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>Username</th>
            <th>Status</th>
            <th>Created</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) { ?>
            <tr>
                <td><?php echo h($user->id) ?></td>
                <td><a href="/user.php?username=<?php echo u($user->username) ?>"><?php echo h($user->username) ?></a></td>
                <td><?php echo h($user->status) ?></td>
                <td><?php echo date('Y-m-d H:i:s', $user->created) ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php include __DIR__ . '/footer.php' ?>
