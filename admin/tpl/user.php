<?php include __DIR__ . '/header.php' ?>

<h1><?php echo h($user->username) ?></h1>

<ul class="nav nav-tabs">
    <li class="active"><a href="/user.php?username=<?php echo u($user->username) ?>">Details</a></li>
    <li><a href="/user-transactions.php?username=<?php echo u($user->username) ?>">Transactions</a></li>
    <li><a href="/user-withdrawals.php?username=<?php echo u($user->username) ?>">Withdrawals</a></li>
</ul>

<table class="table table-bordered">
    <tr>
        <th>ID</th>
        <td><?php echo h($user->id) ?></td>
    </tr>
    <tr>
        <th>Username</th>
        <td><?php echo h($user->username) ?></td>
    </tr>
    <tr>
        <th>Status</th>
        <td>
            <form action="/user-status.php" method="post">
                <input type="hidden" name="user" value="<?php echo h($user->id) ?>" />
                <div class="input-group">
                    <select class="form-control" name="status">
                        <?php foreach ($statuses as $k => $v) { ?>
                            <option value="<?php echo h($k) ?>"<?php if ($k == $user->status) { ?> selected="selected"<?php } ?>><?php echo h($v) ?></option>
                        <?php } ?>
                    </select>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </span>
                </div>
            </form>
        </td>
    </tr>
    <tr>
        <th>Created</th>
        <td><?php echo date('Y-m-d H:i:s', $user->created) ?></td>
    </tr>
</table>

<h2>Accounts</h2>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>Currency</th>
            <th>Address</th>
            <th>Address (emergency)</th>
            <th>Balance</th>
            <th>Adjust balance</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($accounts as $account) { ?>
            <?php $currency = $account->getCurrency() ?>
            <tr>
                <td><?php echo h($account->id) ?></td>
                <td><?php echo h($currency->name) ?></td>
                <td><?php echo h($account->address) ?></td>
                <td><?php echo h($account->emergency_address) ?></td>
                <td style="text-align: right;"><?php echo h($account->balance) ?></td>
                <td>
                    <form action="/user-account-balance.php" method="post">
                        <input type="hidden" name="account" value="<?php echo h($account->id) ?>" />
                        <div class="input-group">
                            <input name="amount" placeholder="0" class="form-control" />
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </span>
                        </div>
                    </form>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<h2>Recent sessions</h2>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>IP</th>
            <th>User Agent</th>
            <th>Created</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($sessions as $session) { ?>
            <tr>
                <td><?php echo h($session->ip) ?></td>
                <td><?php echo h($session->user_agent) ?></td>
                <td><?php echo date('Y-m-d H:i:s', $session->created) ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>


<?php include __DIR__ . '/footer.php' ?>
