<?php include __DIR__ . '/header.php' ?>

<h1><?php echo h($user->username) ?></h1>

<ul class="nav nav-tabs">
    <li><a href="/user.php?username=<?php echo u($user->username) ?>">Details</a></li>
    <li class="active"><a href="/user-transactions.php?username=<?php echo u($user->username) ?>">Transactions</a></li>
    <li><a href="/user-withdrawals.php?username=<?php echo u($user->username) ?>">Withdrawals</a></li>
</ul>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>Type</th>
            <th>Description</th>
            <th>Currency</th>
            <th>Amount</th>
            <th>Created</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($transactions as $transaction) { ?>
            <?php $currency = $transaction->getAccount()->getCurrency() ?>
            <tr>
                <td><?php echo h($transaction->id) ?></td>
                <td><?php echo h($transaction->type) ?></td>
                <td><?php echo h($transaction->description) ?></td>
                <td><?php echo h($currency->name) ?></td>
                <td style="text-align: right;"><?php echo h($transaction->amount) ?></td>
                <td><?php echo date('Y-m-d H:i:s', $transaction->created) ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php include __DIR__ . '/footer.php' ?>
