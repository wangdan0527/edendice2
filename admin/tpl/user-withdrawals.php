<?php include __DIR__ . '/header.php' ?>

<h1><?php echo h($user->username) ?></h1>

<ul class="nav nav-tabs">
    <li><a href="/user.php?username=<?php echo u($user->username) ?>">Details</a></li>
    <li><a href="/user-transactions.php?username=<?php echo u($user->username) ?>">Transactions</a></li>
    <li class="active"><a href="/user-withdrawals.php?username=<?php echo u($user->username) ?>">Withdrawals</a></li>
</ul>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>Currency</th>
            <th>Amount</th>
            <th>Address</th>
            <th>Status</th>
            <th>Created</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($withdrawals as $withdrawal) { ?>
            <?php $currency = $withdrawal->getAccount()->getCurrency() ?>
            <tr>
                <td><?php echo h($withdrawal->id) ?></td>
                <td><?php echo h($currency->name) ?></td>
                <td style="text-align: right;"><?php echo h($withdrawal->amount) ?></td>
                <td><?php echo h($withdrawal->address) ?></td>
                <td><?php echo h($withdrawal->status) ?></td>
                <td><?php echo date('Y-m-d H:i:s', $withdrawal->created) ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php include __DIR__ . '/footer.php' ?>
