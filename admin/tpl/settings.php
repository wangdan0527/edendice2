<?php include __DIR__ . '/header.php' ?>

<h1>Settings</h1>

<form action="settings.php" method="post">
    <table class="table table-bordered">
        <?php foreach ($settings as $setting) { ?>
            <tr>
                <th><?php echo h($setting->name) ?></th>
                <td>
                    <input name="<?php echo h($setting->name) ?>" value="<?php echo h($setting->value) ?>"<?php if ($setting->readonly) { ?> disabled="disabled"<?php } ?> class="form-control" />
                </td>
            </tr>
        <?php } ?>
    </table>
    <button type="submit" class="btn btn-primary">Update</button>
</form>

<?php include __DIR__ . '/footer.php' ?>
