<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/admin.css" />
</head>
<body>
    <nav class="navbar navbar-fixed-top navbar-inverse">
        <div class="navbar-inner">
            <div class="container">
                <ul class="nav navbar-nav">
                    <li><a href="/">Admin</a></li>
                    <li><a href="/settings.php">Settings</a></li>
                    <li><a href="/users.php">Users</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid">
