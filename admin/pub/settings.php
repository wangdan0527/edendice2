<?php

require_once __DIR__ . '/../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    foreach ($_POST as $k => $v) {
        $sql = 'UPDATE settings SET value = ?, modified = UNIX_TIMESTAMP() WHERE name = ? AND readonly = 0';
        \Ed\Db\Db::update($sql, [$v, $k]);
    }
    header('Location: settings.php');
    die;
}

$sql = 'SELECT * FROM settings ORDER BY name';
$res = \Ed\Db\Db::query($sql);
$settings = $res->fetchAll(PDO::FETCH_OBJ);

include __DIR__ . '/../tpl/settings.php';
