<?php

require_once __DIR__ . '/../bootstrap.php';

$sql = 'SELECT * FROM users WHERE username = ?';
$res = \Ed\Db\Db::query($sql, [$_GET['username']]);
$row = $res->fetchObject();
if (!$row) {
    die('User not found.');
}
$user = new \Ed\Model\UserBean($row);

$accounts = $user->getAccounts();

$sessions = [];
$sql = 'SELECT * FROM sessions WHERE user_id = ? ORDER BY created DESC LIMIT 5';
$res = \Ed\Db\Db::query($sql, [$user->id]);
while ($row = $res->fetchObject()) {
    $sessions[] = $row;
}

$statuses = [
    'active' => 'Active',
    'deleted' => 'Deleted',
];

include __DIR__ . '/../tpl/user.php';
