<?php

require_once __DIR__ . '/../bootstrap.php';

$sql = 'SELECT * FROM users ORDER BY id DESC';
$res = \Ed\Db\Db::query($sql);
$users = [];
while ($row = $res->fetchObject()) {
    $users[] = new \Ed\Model\UserBean($row);
}

include __DIR__ . '/../tpl/users.php';
