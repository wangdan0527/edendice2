<?php

require_once __DIR__ . '/../bootstrap.php';

$sql = 'SELECT * FROM users WHERE username = ?';
$res = \Ed\Db\Db::query($sql, [$_GET['username']]);
$row = $res->fetchObject();
if (!$row) {
    http_response_code(404);
    echo 'User not found';
    exit;
}
$user = new \Ed\Model\UserBean($row);

$withdrawals = [];
$sql = 'SELECT withdrawals.* FROM withdrawals INNER JOIN accounts ON withdrawals.account_id = accounts.id WHERE accounts.user_id = ? ORDER BY withdrawals.id DESC';
$res = \Ed\Db\Db::query($sql, [$user->id]);
while ($row = $res->fetchObject()) {
    $withdrawals[] = new \Ed\Model\WithdrawalBean($row);
}

include __DIR__ . '/../tpl/user-withdrawals.php';
