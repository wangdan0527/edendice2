<?php

require_once __DIR__ . '/../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $account = \Ed\Model\Account::findById($_POST['account']);
    if (!$account) {
        http_response_code(404);
        echo 'Account not found.';
        exit;
    }
    $amount = intval($_POST['amount']);
    if ($amount !== 0) {
        \Ed\Model\Transaction::create(
            \Ed\Model\TransactionBean::TYPE_ADJUSTMENT,
            $account->id,
            $amount,
            'Adjustment'
        );
    }

    $sql = 'SELECT * FROM users WHERE id = ?';
    $res = \Ed\Db\Db::query($sql, [$account->user_id]);
    $row = $res->fetchObject();
    header('Location: user.php?username=' . $row->username);
} else {
    http_response_code(405);
    echo 'Method not allowed.';
}
