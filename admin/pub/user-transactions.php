<?php

require_once __DIR__ . '/../bootstrap.php';

$sql = 'SELECT * FROM users WHERE username = ?';
$res = \Ed\Db\Db::query($sql, [$_GET['username']]);
$row = $res->fetchObject();
if (!$row) {
    http_response_code(404);
    echo 'User not found';
    exit;
}
$user = new \Ed\Model\UserBean($row);

$transactions = [];
$sql = 'SELECT transactions.* FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id WHERE accounts.user_id = ? ORDER BY id DESC';
$res = \Ed\Db\Db::query($sql, [$user->id]);
while ($row = $res->fetchObject()) {
    $transactions[] = new \Ed\Model\TransactionBean($row);
}

include __DIR__ . '/../tpl/user-transactions.php';
