<?php

require_once __DIR__ . '/../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $sql = 'SELECT * FROM users WHERE id = ?';
    $res = \Ed\Db\Db::query($sql, [$_POST['user']]);
    $row = $res->fetchObject();
    if (!$row) {
        http_response_code(404);
        echo 'User not found';
        exit;
    }
    $user = new \Ed\Model\UserBean($row);
    $user->changeStatus($_POST['status']);
    header('Location: user.php?username=' . $user->username);
} else {
    http_response_code(405);
    echo 'Method not allowed.';
}
