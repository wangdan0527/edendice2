<?php

require_once __DIR__ . '/../bootstrap.php';

$currencies = \Ed\Model\Currency::findAll();
/** @var \Ed\Model\CurrencyBean $currency */
foreach ($currencies as $currency) {
    $currency->reweightInvestments();
}