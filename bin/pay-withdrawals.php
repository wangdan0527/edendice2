<?php

// */5 * * * * php pay_withdrawals.php

require_once __DIR__ . '/../bootstrap.php';

$withdrawals = \Ed\Model\Withdrawal::findAllNew();
/** @var \Ed\Model\WithdrawalBean $withdrawal */
foreach ($withdrawals as $withdrawal) {
    $withdrawal->pay();
}
