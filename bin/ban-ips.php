<?php

require __DIR__ . '/../bootstrap.php';

if (posix_getuid() !== 0) {
    echo 'This script must be run as root.' . PHP_EOL;
    exit(1);
}

$arg = count($argv) > 1 ? $argv[1] : \Ed\Setting::get('banned_ips');
$ips = \Ed\Util::commaSplit($arg);
\Ed\Net\Iptables::setBannedIps($ips);
