<?php

require_once __DIR__ . '/../bootstrap.php';

\Ed\Db\Db::transaction(function() {
    $currencies = \Ed\Model\Currency::findAll();
    /** @var \Ed\Model\CurrencyBean $currency */
    foreach ($currencies as $currency) {
        $currency->payInvestments();
    }

    $sql = 'UPDATE settings SET value = ?, modified = UNIX_TIMESTAMP() WHERE name = ?';
    \Ed\Db\Db::update($sql, [strval(time()), 'investment_payout_time']);
});

