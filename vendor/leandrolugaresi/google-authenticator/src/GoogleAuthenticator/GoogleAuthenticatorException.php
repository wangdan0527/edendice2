<?php
/**
 * GoogleAuthenticator
 *
 *
 * @package  GoogleAuthenticator
 * @author   Leandro Lugaresi <leandrolugaresi92@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License 3
 * @link     https://github.com/johnstyle/google-authenticator
 */

namespace GoogleAuthenticator;

/**
 * Class GoogleAuthenticatorException
 *
 * @author   Leandro Lugaresi <leandrolugaresi92@gmail.com>
 */
class GoogleAuthenticatorException extends \Exception
{

}
