class components::util {
  define host($ip) {
    exec { "host-$name":
      command => "/bin/grep ' $name' /etc/hosts && sed -i 's/.* $name/$ip $name/' /etc/hosts || echo '$ip $name' >> /etc/hosts",
      unless => "/bin/grep '$ip $name' /etc/hosts",
    }
  }

  service { "cron":
    ensure => running,
  }

  define cron($when, $user, $command) {
    file { "/etc/cron.d/$name":
      content => template("components/cron.erb"),
      mode => 700,
      notify => Service["cron"],
    }
  }
}