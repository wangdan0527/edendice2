class components::php {
  package { ["php5-cli",
             "php5-fpm",
             "php5-curl",
             "php5-mysql",
             "php5-memcached"]:
    ensure => installed,
  }

  service { "php5-fpm":
    ensure => running,
    require => Package["php5-fpm"],
  }
}