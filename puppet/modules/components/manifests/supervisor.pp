class components::supervisor {
  package { "supervisor":
    ensure => installed,
  }

  service { "supervisor":
    ensure => running,
    require => Package["supervisor"],
  }
}