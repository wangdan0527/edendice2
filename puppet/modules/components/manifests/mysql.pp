class components::mysql {
  package { [
    "mysql-server-5.5",
    "mysql-client-5.5"]:
    ensure => installed,
  }

  service { "mysql":
    ensure  => running,
    require => Package["mysql-server-5.5"],
  }
}