class components::node {
  package { ["nodejs",
             "npm"]:
    ensure => installed,
  }

  exec { "install-bower":
    command => "/usr/bin/npm install -g bower",
    require => Package["npm"],
    unless  => "/usr/bin/test -f /usr/local/bin/bower",
  }

  exec { "install-grunt":
    command => "/usr/bin/npm install -g grunt grunt-cli grunt-contrib-concat grunt-contrib-cssmin",
    require => Package["npm"],
    unless  => "/usr/bin/test -f /usr/local/bin/grunt",
  }

  file { "/usr/bin/node":
    target => "/usr/bin/nodejs",
  }
}