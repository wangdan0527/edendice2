class servers::base {
  exec { "apt-get-update":
    command => "/usr/bin/apt-get update",
  }

  Package<| |> -> Exec["apt-get-update"]

  package { ["fail2ban"]:
    ensure => installed,
  }
}