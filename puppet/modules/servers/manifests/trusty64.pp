class servers::trusty64 {
  include servers::base
  include components::memcached
  include components::mysql
  include components::nginx
  include components::node
  include components::php
  include components::supervisor
  include components::util

  file { "/etc/nginx/nginx.conf":
    source => "puppet:///modules/servers/trusty64__nginx__nginx.conf",
    require => Package["nginx"],
    notify => Service["nginx"],
  }

  components::util::host { "dev.edendice.com":
    ip => "127.0.0.1",
  }
  file { "/etc/nginx/sites-enabled/dev.edendice.com.conf":
    source => "puppet:///modules/servers/trusty64__nginx__dev.edendice.com.conf",
    require => Package["nginx"],
    notify => Service["nginx"],
  }

  file { "/etc/nginx/sites-enabled/admin.dev.edendice.com.conf":
    source => "puppet:///modules/servers/trusty64__nginx__admin.dev.edendice.com.conf",
    require => Package["nginx"],
    notify => Service["nginx"],
  }

  components::util::host { "api.dev.edendice.com":
    ip => "127.0.0.1",
  }
  file { "/etc/nginx/sites-enabled/api.dev.edendice.com.conf":
    source => "puppet:///modules/servers/trusty64__nginx__api.dev.edendice.com.conf",
    require => Package["nginx"],
    notify => Service["nginx"],
  }

  file { "/etc/nginx/sites-enabled/default":
    ensure => absent,
    require => Package["nginx"],
    notify => Service["nginx"],
  }

  file { "/etc/supervisor/conf.d/exchange.conf":
    source => "puppet:///modules/servers/trusty64__supervisor__exchange.conf",
    require => Package["supervisor"],
    notify => Service["supervisor"],
  }

  file { "/etc/supervisor/conf.d/stats.conf":
    source => "puppet:///modules/servers/trusty64__supervisor__stats.conf",
    require => Package["supervisor"],
    notify => Service["supervisor"],
  }

  exec { "load-database":
    command => "/usr/bin/mysql -uroot -e 'create database edendice' && mysql -uroot edendice < /usr/local/edendice/edendice.sql",
    require => Package["mysql-server-5.5"],
    unless => "/usr/bin/mysql -uroot -e 'show databases' | grep edendice",
  }

  components::util::cron { "ban-ips":
    when => "* * * * *",
    user => "root",
    command => "/usr/bin/php /usr/local/edendice/bin/ban-ips.php",
  }

  components::util::cron { "pay-affiliates":
    when => "0 0 * * 0",
    user => "root",
    command => "/usr/bin/php /usr/local/edendice/bin/pay-affiliates.php",
  }

  components::util::cron { "pay-investments":
    when => "0 0 * * 0",
    user => "root",
    command => "/usr/bin/php /usr/local/edendice/bin/pay-investments.php",
  }

  components::util::cron { "pay-withdrawals":
    when => "*/5 * * * *",
    user => "root",
    command => "/usr/bin/php /usr/local/edendice/bin/pay-withdrawals.php",
  }

  components::util::cron { "poll-currencies":
    when => "*/5 * * * *",
    user => "root",
    command => "/usr/bin/php /usr/local/edendice/bin/poll-currencies.php",
  }

  components::util::cron { "refresh-stats":
    when => "* * * * *",
    user => "root",
    command => "/usr/bin/php /usr/local/edendice/bin/refresh-stats.php",
  }

  components::util::cron { "reweight-investments":
    when => "0 23 * * 6",
    user => "root",
    command => "/usr/bin/php /usr/local/edendice/bin/reweight-investments.php",
  }

  components::util::cron { "update-currencies":
    when => "* * * * *",
    user => "root",
    command => "/usr/bin/php /usr/local/edendice/bin/update-currencies.php",
  }
}