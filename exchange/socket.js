var winston = require('winston'),
    _ = require('underscore'),
    api = require('./api');

var io;
var sockets = [];

var Socket = function(socket) {
    this.socket = socket;
    this.id = socket.id;
    this.username = null;

    sockets.push(this);

    winston.info('Connect: ' + this.id);
    winston.info('Socket count: ' + sockets.length);

    var instance = this;

    this.socket.on('disconnect', function() {
        sockets = _.without(sockets, _.findWhere(sockets, {id: instance.id}));

        winston.info('Disconnect: ' + instance.id);
        winston.info('Socket count: ' + sockets.length);
    });

    this.socket.on('login', function(token) {
        winston.info('Login: ' + instance.id);
        winston.info('Token: ' + token);

        api.login(token, function(row) {
            winston.info('Logged in as: ' + row.username);
            instance.username = row.username;
        });
    });

    this.socket.on('logout', function() {
        winston.info('Logout: ' + instance.id);

        instance.username = null;
    });

    this.socket.on('chat', function(message) {
        winston.info('Chat: ' + message);

        if (!instance.username) {
            instance.socket.emit('chat error', 'You aren\'t logged in.');
        } else {
            if (message[0] == '/') {
                var tokens = message.split(' ');
                switch (tokens[0]) {
                    case '/w':
                    case '/msg':
                        if (tokens.length < 3) {
                            instance.socket.emit('chat error', 'Syntax: ' + tokens[0] + ' <to> <msg>');
                        } else if (tokens[1] == instance.username) {
                            instance.socket.emit('chat error', 'You can\'t private message yourself.');
                        } else {
                            instance.socket.emit('chat', {
                                from: instance.username,
                                to: tokens[1],
                                message: tokens.slice(2).join(' ')
                            });
                            var f = false;
                            _.each(sockets, function(s) {
                                if (s.username == tokens[1]) {
                                    s.socket.emit('chat', {
                                        from: instance.username,
                                        to: tokens[1],
                                        message: tokens.slice(2).join(' ')
                                    });
                                    f = true;
                                }
                            });
                            if (!f) {
                                instance.socket.emit('chat error', tokens[1] + ' isn\'t online.');
                            }
                        }
                        break;
                    default:
                        instance.socket.emit('chat error', 'Unknown command: ' + tokens[0]);
                        break;
                }
            } else {
                io.emit('chat', {
                    from: instance.username,
                    to: null,
                    message: message
                });
            }
        }
    });

    this.socket.on('error', function(err) {
        winston.error('Error: ' + err);
    });
};

module.exports = function(_io) {
    io = _io;
    return function(socket) {
        new Socket(socket);
    }
};
