var mysql = require('mysql'),
    winston = require('winston');

var mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'edendice'
});

mysqlConnection.connect();

module.exports = {
    login: function(token, fn) {
        mysqlConnection
            .query('SELECT users.* FROM sessions INNER JOIN users ON sessions.user_id = users.id WHERE sessions.token = ?', [token], function(err, rows) {
                if (err) {
                    winston.error('API error: ' + err);
                } else if (rows.length > 0) {
                    fn(rows[0]);
                }
            });
    }
};
