var UDP_PORT = 19817;
var HTTP_PORT = 8080;

var winston = require('winston');
var io = require('socket.io')(HTTP_PORT);
var udp = require('dgram').createSocket('udp4');
var _ = require('underscore');

var socket = require('./socket')(io);

winston.level = 'debug';

//------------------------------------------------------------------------------
// UDP
//------------------------------------------------------------------------------

udp.on('error', function(err) {
    winston.error('UDP error: ' + err);
});

udp.on('message', function(msg) {
    winston.info('UDP message: message');
    var data = JSON.parse(msg);
    var type = data.type;
    data = data.data;
    io.emit(type, data);
});

udp.on('stats', function(msg) {
    windston.info('UDP message: stats');
    var data = JSON.parse(msg);
    var type = data.type;
    data = data.data;
    io.emit(type, data);
});

udp.bind(UDP_PORT);

//------------------------------------------------------------------------------
// Socket.io
//------------------------------------------------------------------------------

io.on('connection', socket);
