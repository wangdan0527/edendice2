<?php

require_once __DIR__ . '/../bootstrap.php';

spl_autoload_register(function($class) {
    $classParts = explode('\\', trim($class, '\\'));
    if ($classParts[0] == 'Api') {
        $classFile = __DIR__ . '/src/' . join('/', $classParts) . '.php';
        require_once $classFile;
    }
});