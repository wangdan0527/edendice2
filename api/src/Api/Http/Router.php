<?php

namespace Api\Http;

use Ed\Http\Route;

class Router extends \Ed\Http\Router {
    public function __construct() {
        $this->addRoute(new Route('/accounts/:account/addresses', 'Api\Http\Handler\AccountAddresses'));
        $this->addRoute(new Route('/accounts/:account/divest', 'Api\Http\Handler\AccountDivest'));
        $this->addRoute(new Route('/accounts/:account/emergency-address', 'Api\Http\Handler\AccountEmergencyAddress'));
        $this->addRoute(new Route('/accounts/:account/invest', 'Api\Http\Handler\AccountInvest'));
        $this->addRoute(new Route('/accounts/:account/withdrawals', 'Api\Http\Handler\AccountWithdrawals'));
        $this->addRoute(new Route('/config.js', 'Api\Http\Handler\Config'));
        $this->addRoute(new Route('/contacts', 'Api\Http\Handler\Contacts'));
        $this->addRoute(new Route('/currencies', 'Api\Http\Handler\Currencies'));
        $this->addRoute(new Route('/games/:game', 'Api\Http\Handler\Game'));
        $this->addRoute(new Route('/leaderboard', 'Api\Http\Handler\Leaderboard'));
        $this->addRoute(new Route('/plays', 'Api\Http\Handler\Plays'));
        $this->addRoute(new Route('/sessions', 'Api\Http\Handler\Sessions'));
        $this->addRoute(new Route('/users', 'Api\Http\Handler\Users'));
        $this->addRoute(new Route('/users/:user', 'Api\Http\Handler\User'));
        $this->addRoute(new Route('/users/:user/accounts', 'Api\Http\Handler\UserAccounts'));
        $this->addRoute(new Route('/users/:user/affiliate', 'Api\Http\Handler\UserAffiliate'));
        $this->addRoute(new Route('/users/:user/keypair', 'Api\Http\Handler\UserKeypair'));
        $this->addRoute(new Route('/users/:user/keypairs', 'Api\Http\Handler\UserKeypairs'));
        $this->addRoute(new Route('/users/:user/mfa', 'Api\Http\Handler\UserMfa'));
        $this->addRoute(new Route('/users/:user/password', 'Api\Http\Handler\UserPassword'));
        $this->addRoute(new Route('/users/:user/transactions', 'Api\Http\Handler\UserTransactions'));
    }
}