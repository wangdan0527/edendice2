<?php

namespace Api\Http\Handler;

use Ed\Http\Exception\NotFound;
use Ed\Http\Exception\Unauthorized;
use Ed\Http\Request;
use Ed\Model\Account;
use Ed\Util;

class AccountEmergencyAddress {
    /**
     * @param Request $request
     * @return null
     * @throws NotFound
     * @throws Unauthorized
     */
    public function post(Request $request) {
        $account = Account::findById($request->getRouteVar('account'));
        if (!$account) {
            throw new NotFound;
        }
        $session = $request->requireSession();
        if ($session->user_id != $account->user_id) {
            throw new Unauthorized;
        }
        $json = $request->getJson();
        $account->setEmergencyAddress(
            Util::okey($json, 'address')
        );
        return null;
    }
}