<?php

namespace Api\Http\Handler;

use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;
use Ed\Model\Play;
use Ed\Util;

class Plays extends AbstractHandler {
    /**
     * @param Request $request
     * @return array
     */
    public function post(Request $request) {
        $session = $request->requireSession();
        $user = $session->getUser();
        $json = $request->getJson();
        $result = Play::play(
            $user->id,
            Util::okey($json, 'game'),
            Util::okey($json, 'account'),
            Util::okey($json, 'bet'),
            $json
        );
        return [
//            'play' => $play->serialize(),
            'result' => $result->serialize(),
            'accounts' => $user->getAccounts()->serialize(),
        ];
    }
}
