<?php

namespace Api\Http\Handler;

use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;
use Ed\Util;

class Contacts extends AbstractHandler {
    /**
     * @param Request $request
     * @return null
     */
    public function post(Request $request) {
        $json = $request->getJson();
        \Ed\Model\Contact::create(
            Util::okey($json, 'name'),
            Util::okey($json, 'email'),
            Util::okey($json, 'phone'),
            Util::okey($json, 'message'),
            Util::okey($json, 'captcha'),
            $request->getRemoteAddr(),
            $request->getUserAgent()
        );
        return null;
    }
}
