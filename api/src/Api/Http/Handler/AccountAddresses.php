<?php

namespace Api\Http\Handler;

use Ed\Http\Exception\NotFound;
use Ed\Http\Exception\Unauthorized;
use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;
use Ed\Model\Account;

class AccountAddresses extends AbstractHandler {
    /**
     * @param Request $request
     * @return array
     * @throws NotFound
     * @throws Unauthorized
     */
    public function post(Request $request) {
        $id = $request->getRouteVar('account');
        $account = Account::findById($id);
        if (!$account) {
            throw new NotFound;
        }
        $session = $request->requireSession();
        if ($session->user_id != $account->user_id) {
            throw new Unauthorized;
        }
        $address = $account->newAddress();
        return [
            'address' => $address,
        ];
    }
}
