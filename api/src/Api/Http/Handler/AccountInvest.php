<?php

namespace Api\Http\Handler;

use Ed\Http\Exception\NotFound;
use Ed\Http\Exception\Unauthorized;
use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;
use Ed\Model\Account;
use Ed\Util;

class AccountInvest extends AbstractHandler {
    /**
     * @param Request $request
     * @return null
     * @throws NotFound
     * @throws Unauthorized
     */
    public function post(Request $request) {
        $account = Account::findById($request->getRouteVar('account'));
        if (!$account) {
            throw new NotFound;
        }
        $session = $request->requireSession();
        if ($session->user_id != $account->user_id) {
            throw new Unauthorized;
        }
        $json = $request->getJson();
        $account->invest(Util::okey($json, 'invest'));
        return [
            'accounts' => $account->getUser()->getAccounts()->serialize(),
        ];
    }
}
