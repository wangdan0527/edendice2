<?php

namespace Api\Http\Handler;

use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;
use Ed\Model\Play;

class Leaderboard extends AbstractHandler {
    /**
     * @param Request $request
     * @return array
     */
    public function get(Request $request) {
        return Play::generateLeaderboard();
    }
}