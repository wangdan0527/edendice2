<?php

namespace Api\Http\Handler;

use Ed\Http\Exception\NotFound;
use Ed\Http\Exception\Unauthorized;
use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;
use Ed\Model\User;

class UserKeypair extends AbstractHandler {
    /**
     * @param Request $request
     * @return array
     * @throws NotFound
     * @throws Unauthorized
     */
    public function get(Request $request) {
        $id = $request->getRouteVar('user');
        $user = User::findById($id);
        if (!$user) {
            throw new NotFound;
        }
        $session = $request->requireSession();
        if ($session->user_id != $user->id) {
            throw new Unauthorized;
        }
        $keypair = $user->getKeypair();
        return $keypair->serialize();
    }
}
