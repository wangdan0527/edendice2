<?php

namespace Api\Http\Handler;

use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;
use Ed\Model\User;
use Ed\Util;

class Users extends AbstractHandler {
    /**
     * @param Request $request
     * @return string
     */
    public function post(Request $request) {
        $json = $request->getJson();
        $user = User::create(
            Util::okey($json, 'username'),
            Util::okey($json, 'password'),
            Util::okey($json, 'password_confirm'),
            $request->getRemoteAddr(),
            $request->getUserAgent(),
            Util::okey($json, 'affiliate_code')
        );
        return $user->serialize();
    }
}
