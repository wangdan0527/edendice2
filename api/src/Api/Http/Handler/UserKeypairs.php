<?php

namespace Api\Http\Handler;

use Ed\Http\Exception\NotFound;
use Ed\Http\Exception\Unauthorized;
use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;
use Ed\Model\User;
use Ed\Util;

class UserKeypairs extends AbstractHandler {
    /**
     * @param Request $request
     * @return array
     * @throws NotFound
     * @throws Unauthorized
     */
    public function post(Request $request) {
        $id = $request->getRouteVar('user');
        $user = User::findById($id);
        if (!$user) {
            throw new NotFound;
        }
        $session = $request->requireSession();
        if ($session->user_id != $user->id) {
            throw new Unauthorized;
        }
        $json = $request->getJson();
        $keypair = $user->newKeypair(Util::okey($json, 'user_key'));
        return $keypair->serialize();
    }
}
