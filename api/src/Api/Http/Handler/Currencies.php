<?php

namespace Api\Http\Handler;

use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;
use Ed\Model\Currency;

class Currencies extends AbstractHandler {
    /**
     * @param Request $request
     * @return array
     */
    public function get(Request $request) {
        $currencies = Currency::findAll();
        return $currencies->serialize();
    }
}
