<?php

namespace Api\Http\Handler;

use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;
use Ed\Model\Session;
use Ed\Util;

class Sessions extends AbstractHandler {
    /**
     * @param Request $request
     * @return string
     */
    public function post(Request $request) {
        $json = $request->getJson();
        $session = Session::create(
            Util::okey($json, 'username'),
            Util::okey($json, 'password'),
            Util::okey($json, 'mfa'),
            $request->getRemoteAddr(),
            $request->getUserAgent()
        );
        return $session->serialize();
    }
}
