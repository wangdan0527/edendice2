<?php

namespace Api\Http\Handler;

use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;
use Ed\Setting;

class Config extends AbstractHandler {
    public function get(Request $request) {
        header('Content-Type: application/json');
        $config = [
            'dice_margin' => Setting::getFloat('dice_margin'),
            'lottery_count' => Setting::getInt('lottery_count'),
            'lottery_max' => Setting::getInt('lottery_max'),
            'lottery_min' => Setting::getInt('lottery_min'),
            'divest_commission' => Setting::getFloat('divest_commission'),
        ];
        echo 'var config = ' . json_encode($config) . ';' . PHP_EOL;
        exit;
    }
}
