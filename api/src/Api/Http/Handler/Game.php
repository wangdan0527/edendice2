<?php

namespace Api\Http\Handler;

use Ed\Http\Exception\NotFound;
use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;

class Game extends AbstractHandler {
    public function get(Request $request) {
        $id = $request->getRouteVar('game');
        $game = \Ed\Model\Game::findById($id);
        if (!$game) {
            throw new NotFound;
        }
        return [
            'id' => intval($game->id),
            'name' => $game->name,
            'instructions' => $game->instructions,
        ];
    }
}
