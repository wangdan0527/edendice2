<?php

namespace Api\Http\Handler;

use Ed\Http\Exception\NotFound;
use Ed\Http\Exception\Unauthorized;
use Ed\Http\Handler\AbstractHandler;
use Ed\Http\Request;
use Ed\Model\User;
use Ed\Setting;

class UserAffiliate extends AbstractHandler {
    /**
     * @param Request $request
     * @return array
     * @throws NotFound
     * @throws Unauthorized
     */
    public function get(Request $request) {
        $id = $request->getRouteVar('user');
        $user = User::findById($id);
        if (!$user) {
            throw new NotFound;
        }
        $session = $request->requireSession();
        if ($session->user_id != $user->id) {
            throw new Unauthorized;
        }
        return [
            'url' => Setting::get('www_url') . '?a=' . $user->affiliate_code,
            'referrals' => $user->getReferralCount(),
            'level' => 'Bronze', // TODO: make this dynamic
            'payout' => 0,
        ];
    }
}