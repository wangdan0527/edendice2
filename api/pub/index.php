<?php

require_once __DIR__ . '/../bootstrap.php';

try {
    $request = new \Ed\Http\Request;
    $router = new \Api\Http\Router;
    $route = $router->match($request->getPath());
    if (!$route) {
        throw new \Ed\Http\Exception\NotFound;
    }
    $res = $route->handle($request);
    $response = new \Ed\Http\Response(200, $res);
} catch (\Ed\Http\Exception\AbstractException $e) {
    $response = new \Ed\Http\Response($e->getCode(), $e->getData());
} catch (\Exception $e) {
    $response = new \Ed\Http\Response(500, $e->getMessage());
}
$response->send();
