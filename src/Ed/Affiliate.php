<?php

namespace Ed;

use Ed\Db\Db;
use Ed\Model\Currency;
use Ed\Model\Transaction;
use Ed\Model\TransactionBean;
use Ed\Model\User;
use Ed\Model\UserBean;

class Affiliate {
    static public function pay() {
        Db::transaction(function() {
            Log::info('Paying affiliates.');

            $payoutTime = Setting::getInt('affiliate_payout_time');

            if ($payoutTime) {
                $sql = 'SELECT SUM(-transactions.amount) AS losses, users.affiliate_user_id, accounts.currency_id FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id INNER JOIN users ON accounts.user_id = users.id WHERE transactions.type IN (?, ?) AND users.status = ? AND users.affiliate_user_id IS NOT NULL AND transactions.created > ? GROUP BY users.affiliate_user_id, accounts.currency_id HAVING losses > 0';
                $res = Db::query($sql, [
                    TransactionBean::TYPE_BET,
                    TransactionBean::TYPE_WIN,
                    UserBean::STATUS_ACTIVE,
                    $payoutTime,
                ]);
            } else {
                $sql = 'SELECT SUM(-transactions.amount) AS losses, users.affiliate_user_id, accounts.currency_id FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id INNER JOIN users ON accounts.user_id = users.id WHERE transactions.type IN (?, ?) AND users.status = ? AND users.affiliate_user_id IS NOT NULL GROUP BY users.affiliate_user_id, accounts.currency_id HAVING losses > 0';
                $res = Db::query($sql, [
                    TransactionBean::TYPE_BET,
                    TransactionBean::TYPE_WIN,
                    UserBean::STATUS_ACTIVE,
                ]);
            }

            while ($row = $res->fetchObject()) {
                $losses = intval($row->losses);
                $currency = Currency::findById($row->currency_id);
                $user = User::findById($row->affiliate_user_id);
                $account = $user->getAccountByCurrency($currency->id);
                if (!$account) {
                    continue;
                }

                $portion = floor(0.1 * $losses); // TODO: make this dependent on the user's level

                Log::info(
                    'Found losses of %d s%s for affiliate user %s. Paying out %d s%s.',
                    $losses,
                    $currency->code,
                    $user->username,
                    $portion,
                    $currency->code
                );

                Transaction::create(
                    TransactionBean::TYPE_AFFILIATE,
                    $account->id,
                    $portion,
                    'Affiliate payout ' . date('Y-m-d')
                );
            }

            $sql = 'UPDATE settings SET value = ?, modified = UNIX_TIMESTAMP() WHERE name = ?';
            Db::update($sql, [time(), 'affiliate_payout_time']);

            Log::info('Done paying affiliates.');
        });
    }
}