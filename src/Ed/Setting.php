<?php

namespace Ed;

use Ed\Db\Db;

class Setting {
    /**
     * @var array
     */
    static protected $settings = [];

    static public function load() {
        $sql = 'SELECT name, value FROM settings';
        $res = Db::query($sql);
        while ($row = $res->fetchObject()) {
            self::$settings[$row->name] = $row->value;
        }
    }

    /**
     * @param string $name
     * @param string $default
     * @return string
     */
    static public function get($name, $default = '') {
        return Util::akey(self::$settings, $name, $default);
    }

    /**
     * @param string $name
     * @return float
     */
    static public function getFloat($name) {
        return floatval(self::get($name, '0.0'));
    }

    /**
     * @param string $name
     * @return int
     */
    static public function getInt($name) {
        return intval(self::get($name, '0'));
    }

    /**
     * @param string $name
     * @return array
     */
    static public function getList($name) {
        $v = self::get($name);
        return array_filter(array_map('trim', explode(',', $v)));
    }
}
