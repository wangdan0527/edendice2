<?php

namespace Ed\Db;

use Ed\Log;

class Db {
    /**
     * @var \PDO
     */
    static protected $pdo;

    /**
     * @var int
     */
    static protected $commitDepth = 0;

    /**
     * @param \PDO $pdo
     */
    static public function setPdo(\PDO $pdo) {
        self::$pdo = $pdo;
    }

    /**
     * @param string $sql
     * @param array $args
     * @return \PDOStatement
     * @throws Exception
     */
    static public function query($sql, array $args = []) {
        $stmt = self::$pdo->prepare($sql);
        if (!$stmt) {
            throw new Exception(self::$pdo->errorInfo()[2]);
        }
        $res = $stmt->execute($args);
        if (!$res) {
            throw new Exception($stmt->errorInfo()[2]);
        }
        return $stmt;
    }

    /**
     * @param string $sql
     * @param array $args
     * @return int
     * @throws Exception
     */
    static public function insert($sql, array $args = []) {
        $stmt = self::$pdo->prepare($sql);
        if (!$stmt) {
            throw new Exception(self::$pdo->errorInfo()[2]);
        }
        $res = $stmt->execute($args);
        if (!$res) {
            throw new Exception($stmt->errorInfo()[2]);
        }
        $stmt->closeCursor();
        return intval(self::$pdo->lastInsertId());
    }

    /**
     * @param string $sql
     * @param array $args
     * @return int
     * @throws Exception
     */
    static public function update($sql, array $args = []) {
        $stmt = self::$pdo->prepare($sql);
        if (!$stmt) {
            throw new Exception(self::$pdo->errorInfo()[2]);
        }
        $res = $stmt->execute($args);
        if (!$res) {
            throw new Exception($stmt->errorInfo()[2]);
        }
        return intval($stmt->rowCount());
    }

    /**
     * @param callable $fn
     * @return mixed
     * @throws \Exception
     */
    static public function transaction(callable $fn) {
        self::$commitDepth ++;
        if (self::$commitDepth == 1) {
            self::$pdo->beginTransaction();
        }
        try {
            $res = $fn();
            self::$commitDepth --;
            if (self::$commitDepth == 0) {
                self::$pdo->commit();
            }
            return $res;
        } catch (\Exception $e) {
            if (self::$commitDepth) {
                self::$commitDepth --;
                throw $e;
            }
            self::$pdo->rollBack();
            throw $e;
        }
    }
}
