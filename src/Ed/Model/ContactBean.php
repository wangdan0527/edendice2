<?php

namespace Ed\Model;

class ContactBean extends AbstractBean {
    /**
     * @var string
     */
    protected $table = 'contacts';
}
