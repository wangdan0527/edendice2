<?php

namespace Ed\Model;

class TransactionBean extends AbstractBean {
    const TYPE_DEPOSIT = 'deposit';
    const TYPE_WITHDRAWAL = 'withdrawal';
    const TYPE_WIN = 'win';
    const TYPE_BET = 'bet';
    const TYPE_ADJUSTMENT = 'adjustment';
    const TYPE_INVESTMENT = 'investment';
    const TYPE_DIVESTMENT = 'divestment';
    const TYPE_AFFILIATE = 'affiliate';

    /**
     * @var string
     */
    protected $table = 'transactions';

    /**
     * @return AccountBean|null
     */
    public function getAccount() {
        return Account::findById($this->account_id);
    }

    /**
     * @return array
     */
    public function serialize() {
        $account = $this->getAccount();
        $currency = $account->getCurrency();
        return [
            'type' => $this->type,
            'currency_name' => $currency->name,
            'amount' => intval($this->amount),
            'description' => $this->description,
            'created' => intval($this->created),
        ];
    }
}
