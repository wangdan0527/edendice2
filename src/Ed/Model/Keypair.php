<?php

namespace Ed\Model;

use Ed\Db\Db;
use Ed\Http\Exception\BadRequest;

class Keypair {
    /**
     * @param int $id
     * @return KeypairBean|null
     */
    static public function findById($id) {
        $sql = 'SELECT * FROM keypairs WHERE id = ?';
        $res = Db::query($sql, [$id]);
        $row = $res->fetchObject();
        return $row ? new KeypairBean($row) : null;
    }

    /**
     * @param string $userKey
     * @return KeypairBean|null
     * @throws BadRequest
     */
    static public function create($userKey) {
        if (!$userKey) {
            throw new BadRequest([
                'user_key' => 'Please enter a value.',
            ]);
        }
        $serverKey = sha1(openssl_random_pseudo_bytes(16));
        $sql = 'INSERT INTO keypairs (user_key, server_key, seq, created) VALUES (?, ?, 1, UNIX_TIMESTAMP())';
        $id = Db::insert($sql, [$userKey, $serverKey]);
        return self::findById($id);
    }
}
