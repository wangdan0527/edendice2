<?php

namespace Ed\Model;

use Ed\Cache\Cache;
use Ed\Db\Db;
use Ed\Game\Result;
use Ed\Http\Exception\BadRequest;
use Ed\Net\Udp;

class Play {
    /**
     * @param int $id
     * @return PlayBean|null
     */
    static public function findById($id) {
        $sql = 'SELECT * FROM plays WHERE id = ?';
        $res = Db::query($sql, [$id]);
        $row = $res->fetchObject();
        return $row ? new PlayBean($row) : null;
    }

    /**
     * @param int $account
     * @param int $game
     * @param Result $result
     * @return PlayBean
     */
    static public function create($account, $game, Result $result) {
        return Db::transaction(function() use($account, $game, $result) {
            $sql = 'INSERT INTO plays (account_id, game_id, keypair_id, keypair_seq, user_play, server_play, result, result_description, created) VALUES (?, ?, ?, ?, ?, ?, ?, ?, UNIX_TIMESTAMP())';
            $id = Db::insert($sql, [
                $account,
                $game,
                $result->getKeypair(),
                $result->getSeq(),
                json_encode($result->getUserPlay()),
                json_encode($result->getServerPlay()),
                $result->getResult(),
                $result->getResultDescription()
            ]);
            return self::findById($id);
        });
    }

    /**
     * @param int $user
     * @param int $game
     * @param int $account
     * @param int $bet
     * @param \stdClass $input
     * @return Result
     */
    static public function play($user, $game, $account, $bet, \stdClass $input) {
        return Db::transaction(function() use($user, $game, $account, $bet, $input) {
            $errors = [];
            if (!$game) {
                $errors['game'] = 'Please specify a game.';
            } else {
                $game = Game::findById($game);
                if (!$game) {
                    $errors['game'] = 'Unknown game.';
                }
            }
            if (!$account) {
                $errors['account'] = 'Please specify an account.';
            } else {
                $account = Account::findById($account);
                if (!$account) {
                    $errors['account'] = 'Unknown account.';
                } elseif ($account->user_id != $user) {
                    $errors['account'] = 'You can\'t play with that account.';
                }
            }
            $bet = intval($bet);
            if ($bet < 0) {
                $errors['bet'] = 'Bet must be non-negative.';
            } elseif ($bet > $account->balance) {
                $errors['bet'] = 'Not enough funds in that account cunt.';
            }
            BadRequest::throwIf($errors);

            $gameEngine = $game->getEngine();
            $result = $gameEngine->play($account->id, $input);

            // Create the play.
            self::create($account->id, $game->id, $result);

            // Create the transaction.  There's one transaction for the bet and
            // one for the win (if applicable).
            Transaction::create(
                TransactionBean::TYPE_BET,
                $account->id,
                -$bet,
                'Bet'
            );
            $payout = floor($bet * $result->getPayout()) + $bet;
            if ($result->getPayout() > 0.0) {
                Transaction::create(
                    TransactionBean::TYPE_WIN,
                    $account->id,
                    $payout,
                    'Win'
                );
            }

            // Broadcast it.
            $user = $account->getUser();
            $currency = $account->getCurrency();
            Udp::broadcast('play', [
                'username' => $user->username,
                'game' => $game->name,
                'currency' => $currency->name,
                'bet' => $bet,
                'payout' => $result->getPayout() > 0.0 ? $payout : 0,
                'user_play' => $result->getUserPlay(),
                'server_play' => $result->getServerPlay(),
            ]);

            return $result;
        });
    }

    /**
     * @return array
     */
    static public function generateLeaderboard()
    {
        return Cache::memoize('leaderboard', 300, function() {
            $leaderboard = [];

            $currencies = Currency::findAll();
            /** @var CurrencyBean $currency */
            foreach ($currencies as $currency) {
                $cur = [
                    'currency' => $currency->name,
                    'day' => [
                        'count' => [],
                        'max' => [],
                    ],
                    'month' => [
                        'count' => [],
                        'max' => [],
                    ],
                    'total' => [
                        'count' => [],
                        'max' => [],
                    ],
                ];

                $start = strtotime('00:00:00');
                // day / count
                $sql = 'SELECT users.username, COUNT(1) AS cnt FROM plays INNER JOIN accounts ON plays.account_id = accounts.id INNER JOIN users ON accounts.user_id = users.id WHERE plays.created > ? AND accounts.currency_id = ? HAVING cnt > 0 ORDER BY cnt DESC LIMIT 10';
                $res = Db::query($sql, [$start, $currency->id]);
                while ($row = $res->fetchObject()) {
                    $cur['day']['count'][] = [$row->username, intval($row->cnt)];
                }
                // day / max
                $sql = 'SELECT users.username, -transactions.amount AS amount FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id INNER JOIN users ON accounts.user_id = users.id WHERE transactions.created > ? AND transactions.type = ? AND accounts.currency_id = ? HAVING amount > 0 ORDER BY amount DESC LIMIT 10';
                $res = Db::query($sql, [
                    $start,
                    TransactionBean::TYPE_BET,
                    $currency->id,
                ]);
                while ($row = $res->fetchObject()) {
                    $cur['day']['max'][] = [$row->username, intval($row->amount)];
                }

                $start = strtotime('first day of this month 00:00:00');
                // month / count
                $sql = 'SELECT users.username, COUNT(1) AS cnt FROM plays INNER JOIN accounts ON plays.account_id = accounts.id INNER JOIN users ON accounts.user_id = users.id WHERE plays.created > ? AND accounts.currency_id = ? HAVING cnt > 0 ORDER BY cnt DESC LIMIT 10';
                $res = Db::query($sql, [$start, $currency->id]);
                while ($row = $res->fetchObject()) {
                    $cur['month']['count'][] = [$row->username, intval($row->cnt)];
                }
                // month / max
                $sql = 'SELECT users.username, -transactions.amount AS amount FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id INNER JOIN users ON accounts.user_id = users.id WHERE transactions.created > ? AND transactions.type = ? AND accounts.currency_id = ? HAVING amount > 0 ORDER BY amount DESC LIMIT 10';
                $res = Db::query($sql, [
                    $start,
                    TransactionBean::TYPE_BET,
                    $currency->id,
                ]);
                while ($row = $res->fetchObject()) {
                    $cur['month']['max'][] = [$row->username, intval($row->amount)];
                }

                // total / count
                $sql = 'SELECT users.username, COUNT(1) AS cnt FROM plays INNER JOIN accounts ON plays.account_id = accounts.id INNER JOIN users ON accounts.user_id = users.id WHERE accounts.currency_id = ? HAVING cnt > 0 ORDER BY cnt DESC LIMIT 10';
                $res = Db::query($sql, [$currency->id]);
                while ($row = $res->fetchObject()) {
                    $cur['total']['count'][] = [$row->username, intval($row->cnt)];
                }
                // total / max
                $sql = 'SELECT users.username, -transactions.amount AS amount FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id INNER JOIN users ON accounts.user_id = users.id WHERE transactions.type = ? AND accounts.currency_id = ? HAVING amount > 0 ORDER BY amount DESC LIMIT 10';
                $res = Db::query($sql, [
                    TransactionBean::TYPE_BET,
                    $currency->id,
                ]);
                while ($row = $res->fetchObject()) {
                    $cur['total']['max'][] = [$row->username, intval($row->amount)];
                }

                $leaderboard[] = $cur;
            }

            return $leaderboard;
        });
    }
}
