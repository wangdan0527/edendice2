<?php

namespace Ed\Model;

use Ed\Db\Db;

class KeypairBean extends AbstractBean {
    /**
     * @var string
     */
    protected $table = 'keypairs';

    /**
     * @return array
     */
    public function serialize() {
        return [
            'user_key_hash' => sha1($this->user_key),
        ];
    }

    /**
     * @param int $seq
     * @return string
     */
    public function getNextHash(&$seq) {
        return Db::transaction(function() use(&$seq) {
            $seq = intval($this->seq);
            $hash = hash_hmac(
                'sha512',
                $this->seq . ':' . $this->user_seed . ':' . $this->seq,
                $this->seq . ':' . $this->server_key . ':' . $this->seq
            );
            $sql = 'UPDATE keypairs SET seq = seq + 1, modified = UNIX_TIMESTAMP() WHERE id = ?';
            Db::update($sql, [$this->id]);
            return $hash;
        });
    }
}
