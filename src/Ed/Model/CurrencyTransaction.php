<?php

namespace Ed\Model;

use Ed\Db\Db;

class CurrencyTransaction {
    /**
     * @param int $currency
     * @param \stdClass $tx
     * @return CurrencyTransactionBean
     */
    static public function create($currency, \stdClass $tx) {
        return Db::transaction(function() use($currency, $tx) {
            $sql = 'INSERT INTO currency_transactions (currency_id, txid, address, category, amount, confirmations, ts, created) VALUES (?, ?, ?, ?, ?, ?, ?, UNIX_TIMESTAMP())';
            Db::query($sql, [
                $currency,
                $tx->txid,
                $tx->address,
                $tx->category,
                // $tx->amount is in BTC
                intval($tx->amount * Currency::CONVERSION_RATIO),
                $tx->confirmations,
                $tx->time,
            ]);
            $currencyTransaction = self::findByCurrencyAndTxid(
                $currency,
                $tx->txid
            );
            $currencyTransaction->createTransaction();
            return $currencyTransaction;
        });
    }

    /**
     * @param int $currency
     * @param string $txid
     * @return CurrencyTransactionBean|null
     */
    static public function findByCurrencyAndTxid($currency, $txid) {
        $sql = 'SELECT * FROM currency_transactions WHERE currency_id = ? AND txid = ?';
        $res = Db::query($sql, [$currency, $txid]);
        $row = $res->fetchObject();
        return $row ? new CurrencyTransactionBean($row) : null;
    }
}
