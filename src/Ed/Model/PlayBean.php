<?php

namespace Ed\Model;

class PlayBean extends AbstractBean {
    const RESULT_WIN = 'win';
    const RESULT_LOSE = 'lose';

    /**
     * @var string
     */
    protected $table = 'plays';

    /**
     * @return array
     */
    public function serialize() {
        return [
            'id' => intval($this->id),
        ];
    }
}
