<?php

namespace Ed\Model;

use Ed\Db\Db;

class Account {
    /**
     * @param int $user
     * @param int $currency
     * @return AccountBean
     */
    static public function create($user, $currency) {
        return Db::transaction(function() use($user, $currency) {
            $sql = 'INSERT INTO accounts (user_id, currency_id, balance, address, created) VALUES (?, ?, 0, ?, UNIX_TIMESTAMP())';
            $id = Db::insert($sql, [$user, $currency, '']);
            $account = self::findById($id);
            try {
                $account->newAddress();
            } catch (\Exception $e) {
            }
            return $account;
        });
    }

    /**
     * @param int $id
     * @return AccountBean|null
     */
    static public function findById($id) {
        $sql = 'SELECT * FROM accounts WHERE id = ?';
        $res = Db::query($sql, [$id]);
        $row = $res->fetchObject();
        return $row ? new AccountBean($row) : null;
    }

    /**
     * @param int $user
     * @param int $currency
     * @return AccountBean|null
     */
    static public function findByUserAndCurrency($user, $currency) {
        $sql = 'SELECT * FROM accounts WHERE user_id = ? AND currency_id = ?';
        $res = Db::query($sql, [$user, $currency]);
        $row = $res->fetchObject();
        return $row ? new AccountBean($row) : null;
    }

    /**
     * @param string $address
     * @return AccountBean|null
     */
    static public function findByAddress($address) {
        $sql = 'SELECT accounts.* FROM account_addresses INNER JOIN accounts ON account_addresses.account_id = accounts.id WHERE account_addresses.address = ?';
        $res = Db::query($sql, [$address]);
        $row = $res->fetchObject();
        return $row ? new AccountBean($row) : null;
    }

    /**
     * @param int $id
     * @return BeanList
     */
    static public function findAllByUser($id) {
        return Db::transaction(function() use($id) {
            $currencies = Currency::findAll();
            $list = new BeanList;
            /** @var CurrencyBean $currency */
            foreach ($currencies as $currency) {
                $account = Account::findByUserAndCurrency($id, $currency->id);
                if (!$account) {
                    $account = self::create($id, $currency->id);
                }
                $list->add($account);
            }
            return $list;
        });
    }
}
