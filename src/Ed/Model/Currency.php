<?php

namespace Ed\Model;

use Ed\Db\Db;

class Currency {
    const CONVERSION_RATIO = 100000000;

    /**
     * @param int $id
     * @return CurrencyBean|null
     */
    static public function findById($id) {
        $sql = 'SELECT * FROM currencies WHERE id = ?';
        $res = Db::query($sql, [$id]);
        $row = $res->fetchObject();
        return $row ? new CurrencyBean($row) : null;
    }
    /**
     * @return BeanList
     */
    static public function findAll() {
        $sql = 'SELECT * FROM currencies';
        $res = Db::query($sql);
        $list = new BeanList;
        while ($row = $res->fetchObject()) {
            $list->add(new CurrencyBean($row));
        }
        return $list;
    }
}
