<?php

namespace Ed\Model;

use Ed\Game\Dice;
use Ed\Game\EdenDice;
use Ed\Game\Lottery;

class GameBean extends AbstractBean {
    /**
     * @var string
     */
    protected $table = 'games';

    /**
     * @return Dice|EdenDice|Lottery|null
     */
    public function getEngine() {
        switch (intval($this->id)) {
            case 1:
                return new Dice;
            case 2:
                return new EdenDice;
            case 3:
                return new Lottery;
            default:
                return null;
        }
    }
}
