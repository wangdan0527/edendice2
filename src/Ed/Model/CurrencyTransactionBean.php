<?php

namespace Ed\Model;

use Ed\Db\Db;
use Ed\Log;

class CurrencyTransactionBean extends AbstractBean {
    /**
     * @var string
     */
    protected $table = 'currency_transactions';

    /**
     * @var array
     */
    protected $primaryKey = ['currency_id', 'txid'];

    /**
     * @return TransactionBean|null
     */
    public function getTransaction() {
        return Transaction::findByCurrencyAndTxid(
            $this->currency_id,
            $this->txid
        );
    }

    /**
     * @return CurrencyBean|null
     */
    public function getCurrency() {
        return Currency::findById($this->currency_id);
    }

    /**
     * @param \stdClass $tx
     */
    public function update(\stdClass $tx) {
        Db::transaction(function() use($tx) {
            $sql = 'UPDATE currency_transactions SET confirmations = ?, modified = UNIX_TIMESTAMP() WHERE currency_id = ? AND txid = ?';
            Db::update($sql, [
                $tx->confirmations,
                $this->currency_id,
                $this->txid,
            ]);
            $this->createTransaction();
        });
    }

    /**
     * @param string $txid
     * @return string
     */
    protected function truncateTxid($txid) {
        return substr($txid, 0, 3) . '...' . substr($txid, -3);
    }

    /**
     * @return TransactionBean|null
     */
    public function createTransaction() {
        return Db::transaction(function() {
            if ($this->category != 'receive') {
                Log::info('Not a "receive" transaction.');
                return null;
            }
            $currency = $this->getCurrency();
            $requiredConfirmations = intval($currency->required_confirmations);
            if (intval($this->confirmations) < $requiredConfirmations) {
                Log::info('Not enough confirmations.');
                return null;
            } elseif ($this->getTransaction()) {
                Log::info('Already have a transaction.');
                return null;
            }
            $account = Account::findByAddress($this->address);
            if (!$account) {
                Log::info('No account found with address %s.', $this->address);
                return null;
            }
            Log::info('Creating transaction.');
            return Transaction::create(
                'deposit',
                $account->id,
                $this->amount,
                'Deposit (txid ' . $this->truncateTxid($this->txid) . ')',
                $this->txid
            );
        });
    }
}
