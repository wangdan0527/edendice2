<?php

namespace Ed\Model;

use Ed\Db\Db;
use Ed\Http\Exception\BadRequest;
use GoogleAuthenticator\GoogleAuthenticator;

class UserBean extends AbstractBean {
    const STATUS_ACTIVE = 'active';
    const STATUS_DELETED = 'deleted';

    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @param string $userKey
     * @return KeypairBean
     */
    public function newKeypair($userKey = '') {
        return Db::transaction(function() use($userKey) {
            if (!$userKey) {
                $userKey = sha1(openssl_random_pseudo_bytes(16));
            }
            $keypair = Keypair::create($userKey);
            $sql = 'UPDATE users SET keypair_id = ?, modified = UNIX_TIMESTAMP() WHERE id = ?';
            Db::update($sql, [$keypair->id, $this->id]);
            return $keypair;
        });
    }

    /**
     * @return KeypairBean|null
     */
    public function getKeypair() {
        if ($this->keypair_id) {
            return Keypair::findById($this->keypair_id);
        } else {
            return $this->newKeypair();
        }
    }

    /**
     * @return BeanList
     */
    public function getAccounts() {
        return Account::findAllByUser($this->id);
    }

    /**
     * @param $currency
     * @return AccountBean|null
     */
    public function getAccountByCurrency($currency) {
        return Account::findByUserAndCurrency($this->id, $currency);
    }

    /**
     * @param string $type
     * @return BeanList
     */
    public function getTransactions($type = '') {
        return Transaction::findAllByUser($this->id, $type);
    }

    /**
     * @param string $currentPassword
     * @param string $newPassword
     * @param string $newPasswordConfirm
     * @return UserBean
     */
    public function changePassword($currentPassword, $newPassword, $newPasswordConfirm) {
        return Db::transaction(function() use($currentPassword, $newPassword, $newPasswordConfirm) {
            $errors = [];
            if (!$currentPassword) {
                $errors['current_password'] = 'Please fill in your current password.';
            }
            if (!$newPassword) {
                $errors['new_password'] = 'Please choose a new password.';
            } elseif (!$newPasswordConfirm) {
                $errors['new_password_confirm'] = 'Please retype your new password.';
            } elseif ($newPassword != $newPasswordConfirm) {
                $errors['new_password'] = 'Your passwords don\'t match.';
            }
            BadRequest::throwIf($errors);

            $sql = 'SELECT password FROM users WHERE id = ?';
            $res = Db::query($sql, [$this->id]);
            $row = $res->fetchObject();
            if (!$row) {
                throw new Exception('User not found.');
            }
            if (!password_verify($currentPassword, $row->password)) {
                BadRequest::throwSingle(
                    'current_password',
                    'Your current password is invalid.'
                );
            }
            $hash = password_hash(
                $newPassword,
                PASSWORD_BCRYPT,
                ['cost' => 12]
            );
            $sql = 'UPDATE users SET password = ?, modified = UNIX_TIMESTAMP() WHERE id = ?';
            Db::query($sql, [$hash, $this->id]);
            return $this;
        });
    }

    /**
     * @param string $status
     * @return UserBean
     */
    public function changeStatus($status) {
        return Db::transaction(function() use($status) {
            if ($status == $this->status) {
                return $this;
            }
            $sql = 'UPDATE users SET status = ?, modified = UNIX_TIMESTAMP() WHERE id = ?';
            Db::update($sql, [$status, $this->id]);
            return $this;
        });
    }

    /**
     * @param bool $mfa
     * @return UserBean
     */
    public function setMfa($mfa) {
        $ga = new GoogleAuthenticator;
        $sql = 'UPDATE users SET mfa = ?, secret = ?, modified = UNIX_TIMESTAMP() WHERE id = ?';
        Db::update($sql, [
            $mfa ? '1' : '0',
            $ga->generateSecretKey(),
            $this->id,
        ]);
        return $this;
    }

    /**
     * @return string
     */
    public function getMfaImgUrl() {
        $ga = new GoogleAuthenticator($this->secret);
        $ga->setIssuer('Eden Dice');
        return $ga->getQRCodeUrl($this->username);
    }

    /**
     * @return array
     */
    public function serialize() {
        return [
            'id' => intval($this->id),
            'username' => $this->username,
            'accounts' => $this->getAccounts()->serialize(),
            'created' => intval($this->created),
        ];
    }

    /**
     * @return int
     */
    public function getReferralCount() {
        $sql = 'SELECT COUNT(1) AS cnt FROM users WHERE affiliate_user_id = ? AND status = ?';
        $res = Db::query($sql, [$this->id, UserBean::STATUS_ACTIVE]);
        $row = $res->fetchObject();
        return intval($row->cnt);
    }
}
