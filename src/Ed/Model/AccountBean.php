<?php

namespace Ed\Model;

use Ed\Db\Db;
use Ed\Http\Exception\BadRequest;
use Ed\Setting;

class AccountBean extends AbstractBean {
    /**
     * @var string
     */
    protected $table = 'accounts';

    /**
     * @return CurrencyBean|null
     */
    public function getCurrency() {
        return Currency::findById($this->currency_id);
    }

    /**
     * @return UserBean|null
     */
    public function getUser() {
        return User::findById($this->user_id);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function newAddress() {
        return Db::transaction(function() {
            $currency = $this->getCurrency();
            $address = $currency->getNewAddress();
            if (!$address) {
                throw new Exception('New address is empty.');
            }

            $sql = 'UPDATE accounts SET address = ?, modified = UNIX_TIMESTAMP() WHERE id = ?';
            Db::update($sql, [$address, $this->id]);

            $sql = 'INSERT INTO account_addresses (account_id, address, created) VALUES (?, ?, UNIX_TIMESTAMP())';
            Db::query($sql, [$this->id, $address]);
            return $address;
        });
    }

    /**
     * @return array
     */
    public function serialize() {
        $currency = $this->getCurrency();
        return [
            'id' => intval($this->id),
            'address' => $this->address,
            'currency_name' => $currency->name,
            'balance' => intval($this->balance),
            'investment' => intval($this->investment),
            'required_confirmations' => intval($currency->required_confirmations),
            'emergency_address' => strval($this->emergency_address),
            'max_payout' => $currency->getMaxPayout(),
            'created' => intval($this->created),
        ];
    }

    /**
     * @param int $amount
     * @param string $address
     * @param string $ip
     * @param string $userAgent
     * @return WithdrawalBean
     */
    public function withdraw($amount, $address, $ip, $userAgent) {
        return Withdrawal::create(
            $this->id,
            $amount,
            $address,
            $ip,
            $userAgent
        );
    }

    /**
     * @param int $amount
     * @return AccountBean
     * @throws BadRequest
     */
    public function invest($amount) {
        return Db::transaction(function() use($amount) {
            $errors = [];
            $amount = intval($amount);
            if ($amount < 1) {
                $errors['invest'] = 'Please enter an amount greater than 0.';
            } elseif ($amount > intval($this->balance)) {
                $errors['invest'] = 'You don\'t have enough funds in this account.';
            }
            BadRequest::throwIf($errors);

            Transaction::create(
                TransactionBean::TYPE_INVESTMENT,
                $this->id,
                -$amount,
                'Investment'
            );

            $sql = 'UPDATE accounts SET investment = investment + ? WHERE id = ?';
            Db::update($sql, [$amount, $this->id]);

            return $this;
        });
    }

    /**
     * @param int $amount
     * @return AccountBean
     * @throws BadRequest
     */
    public function divest($amount) {
        return Db::transaction(function() use($amount) {
            $errors = [];
            $amount = intval($amount);
            if ($amount < 1) {
                $errors['divest'] = 'Please enter an amount greater than 0.';
            } elseif ($amount > intval($this->investment)) {
                $errors['invest'] = 'You don\'t have enough funds invested.';
            }
            BadRequest::throwIf($errors);

            $divestCommission = Setting::getFloat('divest_commission');
            Transaction::create(
                TransactionBean::TYPE_DIVESTMENT,
                $this->id,
                floor($amount * (1 - $divestCommission)),
                'Divestment'
            );

            $sql = 'UPDATE accounts SET investment = investment - ? WHERE id = ?';
            Db::update($sql, [$amount, $this->id]);

            return $this;
        });
    }

    /**
     * @param string $address
     * @return AccountBean
     */
    public function setEmergencyAddress($address) {
        $address = trim(strval($address));
        $sql = 'UPDATE accounts SET emergency_address = ?, modified = UNIX_TIMESTAMP() WHERE id = ?';
        Db::update($sql, [$address, $this->id]);
        return $this;
    }
}
