<?php

namespace Ed\Model;

use Ed\Db\Db;
use Ed\Log;
use Ed\Util;

abstract class AbstractBean {
    /**
     * @var \stdClass
     */
    protected $data;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var array
     */
    protected $primaryKey = ['id'];

    /**
     * @param \stdClass $data
     */
    public function __construct(\stdClass $data) {
        $this->data = $data;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        return Util::okey($this->data, $name);
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value) {
        $this->data->$name = $value;
    }

    /**
     * @return array
     */
    public function serialize() {
        return [];
    }
}
