<?php

namespace Ed\Model;

use Ed\Db\Db;
use Ed\Log;

class Transaction {
    /**
     * @param int $id
     * @return TransactionBean|null
     */
    static public function findById($id) {
        $sql = 'SELECT * FROM transactions WHERE id = ?';
        $res = Db::query($sql, [$id]);
        $row = $res->fetchObject();
        return $row ? new TransactionBean($row) : null;
    }

    /**
     * @param int $currency
     * @param string $txid
     * @return TransactionBean|null
     */
    static public function findByCurrencyAndTxid($currency, $txid) {
        $sql = 'SELECT transactions.* FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id WHERE accounts.currency_id = ? AND transactions.txid = ?';
        $res = Db::query($sql, [$currency, $txid]);
        $row = $res->fetchObject();
        return $row ? new TransactionBean($row) : null;
    }

    /**
     * @param int $user
     * @param string $type
     * @return BeanList
     */
    static public function findAllByUser($user, $type = '') {
        if ($type) {
            $sql = 'SELECT transactions.* FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id WHERE accounts.user_id = ? AND transactions.type = ? ORDER BY transactions.created DESC, transactions.id DESC LIMIT 250';
            $res = Db::query($sql, [$user, $type]);
        } else {
            $sql = 'SELECT transactions.* FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id WHERE accounts.user_id = ? ORDER BY transactions.created DESC, transactions.id DESC LIMIT 250';
            $res = Db::query($sql, [$user]);
        }
        $list = new BeanList();
        while ($row = $res->fetchObject()) {
            $list->add(new TransactionBean($row));
        }
        return $list;
    }

    /**
     * @param string $type
     * @param int $account
     * @param int $amount
     * @param string $description
     * @param string $txid
     * @return TransactionBean
     */
    static public function create($type, $account, $amount, $description, $txid = null) {
        return Db::transaction(function() use($type, $account, $amount, $description, $txid) {
            $sql = 'INSERT INTO transactions (type, account_id, amount, description, txid, created) VALUES (?, ?, ?, ?, ?, UNIX_TIMESTAMP())';
            $id = Db::insert($sql, [
                $type,
                $account,
                $amount,
                $description,
                $txid,
            ]);
            $sql = 'UPDATE accounts SET balance = balance + ?, modified = UNIX_TIMESTAMP() WHERE id = ?';
            Db::query($sql, [$amount, $account]);
            return self::findById($id);
        });
    }
}
