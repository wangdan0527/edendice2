<?php

namespace Ed\Model;

class BeanList implements \Iterator {
    /**
     * @var array
     */
    protected $beans = [];

    /**
     * @param AbstractBean $bean
     * @return BeanList
     */
    public function add(AbstractBean $bean) {
        $this->beans[] = $bean;
        return $this;
    }

    /**
     * @return array
     */
    public function serialize() {
        $return = [];
        /** @var AbstractBean $bean */
        foreach ($this->beans as $bean) {
            $return[] = $bean->serialize();
        }
        return $return;
    }

    //--------------------------------------------------------------------------
    // Iterator
    //--------------------------------------------------------------------------

    /**
     * @var int
     */
    protected $counter = 0;

    /**
     * @return AbstractBean
     */
    public function current()
    {
        return $this->beans[$this->counter];
    }

    public function next()
    {
        $this->counter++;
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->counter;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return isset($this->beans[$this->counter]);
    }

    public function rewind()
    {
        $this->counter = 0;
    }
}
