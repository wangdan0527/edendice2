<?php

namespace Ed\Model;

use Ed\Db\Db;
use Ed\Log;

class WithdrawalBean extends AbstractBean {
    const STATUS_ERROR = 'error';
    const STATUS_PAID = 'paid';

    /**
     * @var string
     */
    protected $table = 'withdrawals';

    /**
     * @return AccountBean|null
     */
    public function getAccount() {
        return Account::findById($this->account_id);
    }

    /**
     * @param string $status
     * @return WithdrawalBean
     */
    public function setStatus($status) {
        if ($this->status == $status) {
            return $this;
        }
        $sql = 'UPDATE withdrawals SET status = ?, modified = UNIX_TIMESTAMP() WHERE id = ?';
        Db::update($sql, [$status, $this->id]);
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function pay() {
        Db::transaction(function() {
            if ($this->status != 'new') {
                Log::info(
                    'Can\'t pay withdrawal %d - it\'s %s.',
                    $this->id,
                    $this->status
                );
            }
            $account = $this->getAccount();
            $currency = $account->getCurrency();
            try {
                $serverComment = sprintf(
                    'Withdrawal from account %d to %s',
                    $this->account_id,
                    $this->address
                );
                $currency->pay(
                    $this->amount,
                    $this->address,
                    $serverComment,
                    'Withdrawal from Eden Dice'
                );
                $this->setStatus(self::STATUS_PAID);
            } catch (\Exception $e) {
                $this->setStatus(self::STATUS_ERROR);
            }
        });
    }
}
