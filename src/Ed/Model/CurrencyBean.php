<?php

namespace Ed\Model;

use Ed\Db\Db;
use Ed\Log;
use Ed\Net\Udp;
use Ed\Setting;
use Nbobtc\Command\Command;
use Nbobtc\Http\Client;

class CurrencyBean extends AbstractBean {
    /**
     * @var string
     */
    protected $table = 'currencies';

    /**
     * @var Client
     */
    protected $client;

    /**
     * @return Client
     */
    protected function getClient() {
        if (!$this->client) {
            $url = Setting::get($this->setting_prefix . '_url');
            $this->client = new Client($url);
        }
        return $this->client;
    }

    /**
     * @param string $name
     * @param array $params
     * @return mixed
     * @throws Exception
     */
    public function command($name, array $params = []) {
        $command = new Command($name, $params);
        $client = $this->getClient();
        $res = $client->sendCommand($command);
        $body = $res->getBody()->getContents();
        $data = json_decode($body);
        if (!$data || !isset($data->result)) {
            throw new Exception('Error executing "' . $name . '" (received "' . $body . '").');
        }
        return $data->result;
    }

    /**
     * @return string
     */
    public function getNewAddress() {
        return $this->command('getnewaddress');
    }

    /**
     * @return array
     */
    public function getAllAddresses() {
        return $this->command('getaddressesbyaccount', ['']);
    }

    /**
     * @return array
     */
    public function getAllTransactions() {
        $count = 50;
        $skip = 0;
        $return = [];
        do {
            $transactions = $this->command(
                'listtransactions',
                ['', $count, $skip]
            );
            $return = array_merge($return, $transactions);
            $skip += $count;
        } while(count($transactions) > 0);
        return $return;
    }

    /**
     * @return CurrencyBean
     */
    public function poll() {
        return Db::transaction(function() {
            Log::info('Polling %s.', $this->name);

            $transactions = $this->getAllTransactions();
            foreach ($transactions as $tx) {
                Log::info('Found tx %s.', $tx->txid);
                $currencyTransaction = CurrencyTransaction::findByCurrencyAndTxid(
                    $this->id,
                    $tx->txid
                );
                if ($currencyTransaction) {
                    $currencyTransaction->update($tx);
                } else {
                    CurrencyTransaction::create($this->id, $tx);
                }
            }

            Log::info('Done polling.');
        });
    }

    /**
     * @param int $amount
     * @param string $address
     * @param string $serverComment
     * @param string $userComment
     * @return string The txid
     */
    public function pay($amount, $address, $serverComment, $userComment) {
        $btcAmount = $amount / Currency::CONVERSION_RATIO;
        $btcAmount = floatval(sprintf('%0.8f', $btcAmount));
        $fee =
            Setting::getInt($this->setting_prefix . '_tx_fee') /
            Currency::CONVERSION_RATIO;
        $this->command('settxfee', [$fee]);
        $txid = $this->command('sendtoaddress', [
            $address,
            $btcAmount,
            $serverComment,
            $userComment,
        ]);
        Log::info(
            'Paid %d s%s to %s, txid %s.',
            $amount,
            $this->code,
            $address,
            $txid
        );
        return $txid;
    }

    /**
     * @return CurrencyBean
     */
    public function updateBalance() {
        return Db::transaction(function() {
            // Balance.
            $balance = floatval($this->command('getbalance'));
            $balance *= Currency::CONVERSION_RATIO;
            $sql = 'UPDATE currencies SET balance = ?, modified = UNIX_TIMESTAMP() WHERE id = ?';
            Db::update($sql, [$balance, $this->id]);

            // Profit.
            $sql = 'SELECT -SUM(transactions.amount) AS profit FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id WHERE transactions.type IN (?, ?) AND accounts.currency_id = ?';
            $res = Db::query($sql, [
                TransactionBean::TYPE_BET,
                TransactionBean::TYPE_WIN,
                $this->id,
            ]);
            $row = $res->fetchObject();
            $profit = intval($row->profit);
            $sql = 'UPDATE currencies SET profit = ?, modified = UNIX_TIMESTAMP() WHERE id = ?';
            Db::update($sql, [$profit, $this->id]);

            // Broadcast.
            Udp::broadcast('currency', [
                'name' => $this->name,
                'balance' => $balance,
                'profit' => $profit,
            ]);

            Log::info(
                'Updated %s. Balance: %d. Profit: %d.',
                $this->name,
                $balance,
                $profit
            );

            return $this;
        });
    }

    /**
     * @return CurrencyBean
     */
    public function reweightInvestments()
    {
        return Db::transaction(function() {
            Log::info('Reweighting investments for %s.', $this->name);

            $sql = 'SELECT SUM(investment) AS investment FROM accounts WHERE currency_id = ?';
            $res = Db::query($sql, [$this->id]);
            $row = $res->fetchObject();
            $investment = intval($row->investment);

            if ($investment > 0) {
                $sql = 'UPDATE accounts SET investment_weighting = investment / ?, modified = UNIX_TIMESTAMP() WHERE currency_id = ?';
                Db::update($sql, [$investment, $this->id]);
            } else {
                $sql = 'UPDATE accounts SET investment_weighting = 0, modified = UNIX_TIMESTAMP() WHERE currency_id = ?';
                Db::update($sql, [$this->id]);
            }

            Log::info('Done reweighting investments.');

            return $this;
        });
    }

    /**
     * @return array
     */
    public function serialize() {
        return [
            'id' => intval($this->id),
            'name' => $this->name,
            'balance' => intval($this->balance),
            'profit' => intval($this->profit),
            'max_payout' => $this->getMaxPayout(),
        ];
    }

    /**
     * @return int
     */
    public function getMaxPayout() {
        return floor(Setting::getFloat('max_payout_ratio') * intval($this->balance));
    }

    /**
     * @return CurrencyBean
     */
    public function payInvestments() {
        return Db::transaction(function() {
            Log::info('Paying investments for %s.', $this->name);

            $previous = Setting::getInt('investment_payout_time');
            if ($previous) {
                $sql = 'SELECT -SUM(transactions.amount) AS profit FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id WHERE transactions.type IN (?, ?) AND accounts.currency_id = ? AND transactions.created > ?';
                $res = Db::query($sql, [
                    TransactionBean::TYPE_BET,
                    TransactionBean::TYPE_WIN,
                    $this->id,
                    $previous,
                ]);
            } else {
                $sql = 'SELECT -SUM(transactions.amount) AS profit FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id WHERE transactions.type IN (?, ?) AND accounts.currency_id = ?';
                $res = Db::query($sql, [
                    TransactionBean::TYPE_BET,
                    TransactionBean::TYPE_WIN,
                    $this->id,
                ]);
            }
            $row = $res->fetchObject();
            $profit = intval($row->profit);
            Log::info('Profit is %d.', $profit);
            $payout = floor(Setting::getFloat('investment_payout') * $profit);
            Log::info('Payout is %d.', $payout);
            if ($payout) { // i.e. != 0
                $sql = 'UPDATE accounts SET investment = GREATEST(0, investment + FLOOR(investment_weighting * ?)), modified = UNIX_TIMESTAMP() WHERE currency_id = ? AND investment_weighting > 0';
                $rows = Db::update($sql, [$payout, $this->id]);
                Log::info('Divided %d between %d user(s).', $payout, $rows);
            }

            $this->reweightInvestments();

            Log::info('Done paying investments.');

            return $this;
        });
    }
}
