<?php

namespace Ed\Model;

class SessionBean extends AbstractBean {
    /**
     * @var string
     */
    protected $table = 'sessions';

    /**
     * @return UserBean|null
     */
    public function getUser() {
        return User::findById($this->user_id);
    }

    /**
     * @return array
     */
    public function serialize() {
        $user = $this->getUser();
        return [
            'token' => $this->token,
            'user_id' => intval($user->id),
            'username' => $user->username,
            'created' => intval($this->created),
        ];
    }
}
