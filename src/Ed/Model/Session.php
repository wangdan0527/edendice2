<?php

namespace Ed\Model;

use Ed\Db\Db;

class Session {
    /**
     * @param string $username
     * @param string $password
     * @param string $mfa
     * @param string $ip
     * @param string $userAgent
     * @return SessionBean
     */
    static public function create($username, $password, $mfa, $ip, $userAgent) {
        return Db::transaction(function() use($username, $password, $mfa, $ip, $userAgent) {
            $user = User::login($username, $password, $mfa);
            $sql = 'INSERT INTO sessions (user_id, token, ip, user_agent, created) VALUES (?, ?, ?, ?, UNIX_TIMESTAMP())';
            $token = self::nextToken();
            Db::query($sql, [$user->id, $token, $ip, $userAgent]);
            return self::findByToken($token);
        });
    }

    /**
     * @return string
     */
    static protected function nextToken() {
        return Db::transaction(function() {
            while (true) {
                $token = bin2hex(openssl_random_pseudo_bytes(16));
                $sql = 'SELECT * FROM sessions WHERE token = ?';
                $res = Db::query($sql, [$token]);
                $row = $res->fetchObject();
                if (!$row) {
                    return $token;
                }
            }
            return '';
        });
    }

    /**
     * @param string $token
     * @return SessionBean|null
     */
    static public function findByToken($token) {
        $sql = 'SELECT * FROM sessions WHERE token = ?';
        $res = Db::query($sql, [$token]);
        $row = $res->fetchObject();
        return $row ? new SessionBean($row) : null;
    }
}
