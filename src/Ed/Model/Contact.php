<?php

namespace Ed\Model;

use Captcha\Captcha;
use Ed\Db\Db;
use Ed\Http\Exception\BadRequest;
use Ed\Setting;

class Contact {
    /**
     * @param string $name
     * @param string $email
     * @param string $phone
     * @param string $message
     * @param string $ip
     * @param string $userAgent
     * @return ContactBean
     */
    static public function create($name, $email, $phone, $message, $captcha, $ip, $userAgent) {
        return Db::transaction(function() use($name, $email, $phone, $message, $captcha, $ip, $userAgent) {
            $errors = [];
            $name = trim($name);
            if (!$name) {
                $errors['name'] = 'Please fill in your name.';
            }
            $email = trim($email);
            if (!$email) {
                $errors['email'] = 'Please fill in your email address.';
            }
            $phone = trim($phone);
            $message = trim($message);
            if (!$message) {
                $errors['message'] = 'Please fill in your message.';
            }
            if (!$captcha || !$captcha instanceof \stdClass) {
                $errors['captcha'] = 'Please confirm you\'re human.';
            }
            $recaptcha = new Captcha;
            $recaptcha->setPublicKey(Setting::get('captcha_public_key'));
            $recaptcha->setPrivateKey(Setting::get('captcha_secret_key'));
            if ($ip) {
                $recaptcha->setRemoteIp($ip);
            }
            $response = $recaptcha->check($captcha->challenge, $captcha->response);
            if (!$response->isValid()) {
                $errors['captcha'] = 'Wrong!';
            }

            BadRequest::throwIf($errors);

            $sql = 'INSERT INTO contacts (name, email, phone, message, ip, user_agent, created) VALUES (?, ?, ?, ?, ?, ?, UNIX_TIMESTAMP())';
            $id = Db::insert($sql, [
                $name, $email, $phone, $message, $ip, $userAgent,
            ]);
            return self::findById($id);
        });
    }

    /**
     * @param int $id
     * @return ContactBean|null
     */
    static public function findById($id) {
        $sql = 'SELECT * FROM contacts WHERE id = ?';
        $res = Db::query($sql, [$id]);
        $row = $res->fetchObject();
        return $row ? new ContactBean($row) : null;
    }
}
