<?php

namespace Ed\Model;

use Ed\Db\Db;

class Game {
    /**
     * @param int $id
     * @return GameBean|null
     */
    static public function findById($id) {
        $sql = 'SELECT * FROM games WHERE id = ?';
        $res = Db::query($sql, [$id]);
        $row = $res->fetchObject();
        return $row ? new GameBean($row) : null;
    }
}
