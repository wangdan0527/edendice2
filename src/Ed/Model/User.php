<?php

namespace Ed\Model;

use Ed\Db\Db;
use Ed\Http\Exception\BadRequest;
use GoogleAuthenticator\GoogleAuthenticator;

class User {
    /**
     * @param string $username
     * @param string $password
     * @param string $passwordConfirm
     * @param string $ip
     * @param string $userAgent
     * @param string $affiliateCode
     * @return UserBean
     */
    static public function create($username, $password, $passwordConfirm, $ip, $userAgent, $affiliateCode) {
        return Db::transaction(function() use($username, $password, $passwordConfirm, $ip, $userAgent, $affiliateCode) {
            $errors = [];
            $username = trim($username);
            if (!$username) {
                $errors['username'] = 'Please choose a username.';
            } else {
                $testUser = self::findByUsername($username);
                if ($testUser) {
                    $errors['username'] = 'That username is already registered.';
                }
            }
            if (!$password) {
                $errors['password'] = 'Please choose a password.';
            } elseif (!$passwordConfirm) {
                $errors['password_confirm'] = 'Please retype your password.';
            } elseif ($password != $passwordConfirm) {
                $errors['password'] = 'Your passwords don\'t match.';
            }
            BadRequest::throwIf($errors);

            $hash = password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);
            $ga = new GoogleAuthenticator;
            $newAffiliateCode = bin2hex(openssl_random_pseudo_bytes(8));
            $affiliateUser = self::findByAffiliateCode($affiliateCode);
            $sql = 'INSERT INTO users (username, password, secret, affiliate_code, affiliate_user_id, created) VALUES (?, ?, ?, ?, ?, UNIX_TIMESTAMP())';
            $id = Db::insert($sql, [
                $username,
                $hash,
                $ga->generateSecretKey(),
                $newAffiliateCode,
                $affiliateUser ? $affiliateUser->id : null,
            ]);
            $user = self::findById($id);
            $user->newKeypair();
            return $user;
        });
    }

    /**
     * @param string $username
     * @param string $password
     * @param string $mfa
     * @return UserBean
     * @throws BadRequest
     */
    static public function login($username, $password, $mfa) {
        $errors = [];
        if (!$username) {
            $errors['username'] = 'Please fill in your username.';
        }
        if (!$password) {
            $errors['password'] = 'Please fill in your password.';
        }
        BadRequest::throwIf($errors);

        $sql = 'SELECT * FROM users WHERE username = ?';
        $res = Db::query($sql, [$username]);
        $row = $res->fetchObject();
        if (!$row || !password_verify($password, $row->password)) {
            BadRequest::throwSingle('username', 'Invalid username / password.');
        } elseif (boolval(intval($row->mfa))) {
            $ga = new GoogleAuthenticator($row->secret);
            if (!$ga->verifyCode($mfa)) {
                BadRequest::throwSingle('username', 'Invalid username / password.');
            }
        }
        if ($row->status == UserBean::STATUS_DELETED) {
            BadRequest::throwSingle('username', 'Your account has been locked.');
        }
        return new UserBean($row);
    }

    /**
     * @param int $id
     * @return UserBean|null
     */
    static public function findById($id) {
        $sql = 'SELECT * FROM users WHERE id = ?';
        $res = Db::query($sql, [$id]);
        $row = $res->fetchObject();
        return $row ? new UserBean($row) : null;
    }

    /**
     * @param string $username
     * @return UserBean|null
     */
    static public function findByUsername($username) {
        $sql = 'SELECT * FROM users WHERE username = ?';
        $res = Db::query($sql, [$username]);
        $row = $res->fetchObject();
        return $row ? new UserBean($row) : null;
    }

    /**
     * @param string $code
     * @return UserBean|null
     */
    static public function findByAffiliateCode($code) {
        $sql = 'SELECT * FROM users WHERE affiliate_code = ? AND status = ?';
        $res = Db::query($sql, [$code, UserBean::STATUS_ACTIVE]);
        $row = $res->fetchObject();
        return $row ? new UserBean($row) : null;
    }
}
