<?php

namespace Ed\Model;

class KeypairHash {
    /**
     * @var int
     */
    protected $keypair;

    /**
     * @var string
     */
    protected $hash;

    /**
     * @var int
     */
    protected $seq;

    /**
     * @param int $keypair
     * @param string $hash
     * @param int $seq
     */
    public function __construct($keypair, $hash, $seq) {
        $this->keypair = intval($keypair);
        $this->hash = strval($hash);
        $this->seq = intval($seq);
    }

    /**
     * @return int
     */
    public function getKeypair() {
        return $this->keypair;
    }

    /**
     * @return string
     */
    public function getHash() {
        return $this->hash;
    }

    /**
     * @return int
     */
    public function getSeq() {
        return $this->seq;
    }
}
