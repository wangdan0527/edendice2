<?php

namespace Ed\Model;

use Ed\Db\Db;
use Ed\Http\Exception\BadRequest;
use Ed\Setting;
use Ed\Sms;

class Withdrawal {
    /**
     * @param $id
     * @return WithdrawalBean|null
     */
    static public function findById($id) {
        $sql = 'SELECT * FROM withdrawals WHERE id = ?';
        $res = Db::query($sql, [$id]);
        $row = $res->fetchObject();
        return $row ? new WithdrawalBean($row) : null;
    }

    /**
     * @return BeanList
     */
    static public function findAllNew() {
        $sql = 'SELECT * FROM withdrawals WHERE status = ? ORDER BY id';
        $res = Db::query($sql, ['new']);
        $list = new BeanList();
        while ($row = $res->fetchObject()) {
            $list->add(new WithdrawalBean($row));
        }
        return $list;
    }

    /**
     * @param int $account
     * @param int $amount
     * @param string $address
     * @param string $ip
     * @param string $userAgent
     * @return WithdrawalBean
     */
    static public function create($account, $amount, $address, $ip, $userAgent) {
        return Db::transaction(function() use($account, $amount, $address, $ip, $userAgent) {
            $account = Account::findById($account);
            $errors = [];
            $amount = intval($amount);
            if ($amount < 1) {
                $errors['amount'] = 'Please enter an amount greater than 0.';
            } elseif ($amount > $account->balance) {
                $errors['amount'] = 'There aren\'t enough funds in the account to withdraw that amount.';
            } else {
                $currency = $account->getCurrency();
                $txfee = Setting::getInt($currency->setting_prefix . '_tx_fee');
                if ($amount + $txfee > $account->balance) {
                    $errors['amount'] = 'There aren\'t enough funds in the account to withdraw that amount and pay the transaction fee.';
                }
            }
            if (!$address) {
                $errors['address'] = 'Please enter the address to withdraw to.';
            }
            BadRequest::throwIf($errors);

            $sql = 'INSERT INTO withdrawals (account_id, amount, address, status, ip, user_agent, created) VALUES (?, ?, ?, ?, ?, ?, UNIX_TIMESTAMP())';
            $id = Db::insert($sql, [
                $account->id,
                $amount,
                $address,
                'new',
                $ip,
                $userAgent,
            ]);
            Transaction::create('withdrawal', $account->id, -$amount, 'Withdrawal to ' . $address);

            // SMS?
            $user = $account->getUser();
            $currency = $account->getCurrency();
            $settingPrefix = $currency->setting_prefix;
            $smsWithdrawalAmount = Setting::getInt($settingPrefix . '_withdrawal_sms_amount');
            if ($amount >= $smsWithdrawalAmount) {
                $recipients = Setting::getList('admin_sms');
                Sms::send($recipients, sprintf(
                    '%s is has requested a withdrawal of %d µ%s',
                    $user->username,
                    $amount,
                    $currency->code
                ));
            }

            return self::findById($id);
        });
    }
}
