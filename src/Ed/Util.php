<?php

namespace Ed;

class Util {
    /**
     * @param array $array
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    static public function akey(array &$array, $key, $default = null) {
        return array_key_exists($key, $array) ? $array[$key] : $default;
    }

    /**
     * @param \stdClass $obj
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    static public function okey(\stdClass &$obj, $key, $default = null) {
        return isset($obj->$key) ? $obj->$key : $default;
    }

    /**
     * @param string $str
     * @return array
     */
    static public function commaSplit($str) {
        return array_filter(array_map('trim', explode(',', $str)));
    }

    /**
     * @param array $array
     * @param array $except
     * @return array
     */
    static public function arrayExcept(array $array, array $except) {
        return array_diff_key($array, array_flip($except));
    }
}
