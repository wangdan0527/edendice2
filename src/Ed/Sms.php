<?php

namespace Ed;

use Clickatell\Api\ClickatellHttp;

class Sms {
    /**
     * @param array $to
     * @param string $message
     */
    static public function send(array $to, $message) {
        $clickatell = new ClickatellHttp(
            Setting::get('clickatell_username'),
            Setting::get('clickatell_password'),
            Setting::get('clickatell_api_id')
        );
        $return = $clickatell->sendMessage($to, $message);
        foreach ($return as $r) {
            if ($r->error) {
                Log::error(
                    'Error sending SMS to %s: %s.',
                    $r->destination,
                    $r->error
                );
            } else {
                Log::info('Sent SMS to %s.', $r->destination);
            }
        }
    }
}