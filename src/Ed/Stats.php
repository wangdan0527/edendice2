<?php

namespace Ed;

use Ed\Db\Db;
use Ed\Model\PlayBean;
use Ed\Model\TransactionBean;
use Ed\Net\Udp;

class Stats {
    static public function refresh() {
        $stats = [];

        // Total wins
        $sql = 'SELECT COUNT(1) AS cnt FROM plays WHERE result != ?';
        $res = Db::query($sql, [PlayBean::RESULT_LOSE]);
        $row = $res->fetchObject();
        $stats['total_wins'] = intval($row->cnt);

        // Total plays
        $sql = 'SELECT COUNT(1) AS cnt FROM plays';
        $res = Db::query($sql);
        $row = $res->fetchObject();
        $stats['total_plays'] = intval($row->cnt);

        // Bitcoin wagered
        $sql = 'SELECT -SUM(transactions.amount) AS amount FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id WHERE transactions.type = ? AND accounts.currency_id = 1';
        $res = Db::query($sql, [TransactionBean::TYPE_BET]);
        $row = $res->fetchObject();
        $stats['bitcoin_wagered'] = intval($row->amount);

        // Litecoin wagered
        $sql = 'SELECT -SUM(transactions.amount) AS amount FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id WHERE transactions.type = ? AND accounts.currency_id = 2';
        $res = Db::query($sql, [TransactionBean::TYPE_BET]);
        $row = $res->fetchObject();
        $stats['litecoin_wagered'] = intval($row->amount);

        // Bitcoin profit
        $sql = 'SELECT -SUM(transactions.amount) AS amount FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id WHERE transactions.type IN (?, ?) AND accounts.currency_id = 1';
        $res = Db::query($sql, [TransactionBean::TYPE_BET, TransactionBean::TYPE_WIN]);
        $row = $res->fetchObject();
        $stats['bitcoin_profit'] = intval($row->amount);

        // Litecoin profit
        $sql = 'SELECT -SUM(transactions.amount) AS amount FROM transactions INNER JOIN accounts ON transactions.account_id = accounts.id WHERE transactions.type IN (?, ?) AND accounts.currency_id = 2';
        $res = Db::query($sql, [TransactionBean::TYPE_BET, TransactionBean::TYPE_WIN]);
        $row = $res->fetchObject();
        $stats['litecoin_profit'] = intval($row->amount);

        // Bitcoin plays
        $sql = 'SELECT COUNT(1) AS cnt FROM plays INNER JOIN accounts ON plays.account_id = accounts.id WHERE accounts.currency_id = 1';
        $res = Db::query($sql);
        $row = $res->fetchObject();
        $stats['bitcoin_plays'] = intval($row->cnt);

        // Litecoin plays
        $sql = 'SELECT COUNT(1) AS cnt FROM plays INNER JOIN accounts ON plays.account_id = accounts.id WHERE accounts.currency_id = 2';
        $res = Db::query($sql);
        $row = $res->fetchObject();
        $stats['litecoin_plays'] = intval($row->cnt);

        Udp::broadcast('stats', $stats);
    }
}