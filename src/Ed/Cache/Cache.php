<?php

namespace Ed\Cache;

use Ed\Setting;

class Cache {
    /**
     * @var \Memcached
     */
    static protected $memcached;

    /**
     * @return \Memcached
     */
    static public function getClient() {
        if (!self::$memcached) {
            self::$memcached = new \Memcached();
            $servers = Setting::getList('memcached_servers');
            foreach ($servers as $server) {
                list($host, $port) = explode(':', $server);
                self::$memcached->addServer($host, intval($port));
            }
        }
        return self::$memcached;
    }

    /**
     * @param string $name
     * @param int $ttl
     * @param callable $fn
     * @return mixed
     */
    static public function memoize($name, $ttl, $fn) {
        $c = self::getClient();
        if ($val = $c->get($name)) {
            return $val;
        }
        $val = $fn();
        $c->set($name, $val, time() + $ttl);
        return $val;
    }
}