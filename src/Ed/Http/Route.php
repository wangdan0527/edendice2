<?php

namespace Ed\Http;

use Ed\Http\Exception\MethodNotAllowed;
use Ed\Util;

class Route {
    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $handler;

    /**
     * @var array
     */
    protected $vars = [];

    /**
     * @param string $path
     * @param string $handler
     */
    public function __construct($path, $handler) {
        $this->path = $path;
        $this->handler = $handler;
    }

    /**
     * @return string
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getPathRegexp() {
        return
            '#^' .
            preg_replace('/:([a-z]+)/', '(?P<\1>[^/]+)', $this->path) .
            '$#';
    }

    /**
     * @return string
     */
    public function getHandler() {
        return $this->handler;
    }

    /**
     * @param string $path
     * @return bool
     */
    public function match($path) {
        $matches = [];
        $match = preg_match($this->getPathRegexp(), $path, $matches);
        if ($match) {
            $this->vars = $matches;
        }
        return boolval($match);
    }

    /**
     * @param string $name
     * @return string
     */
    public function getVar($name) {
        return Util::akey($this->vars, $name);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws MethodNotAllowed
     */
    public function handle(Request $request) {
        $className = $this->handler;
        /** @var \Ed\Http\Handler\AbstractHandler $handler */
        $handler = new $className;
        $request->setRoute($this);
        switch ($request->getMethod()) {
            case 'GET':
                return $handler->get($request);
            case 'POST':
                return $handler->post($request);
            case 'DELETE':
                return $handler->delete($request);
        }
        throw new MethodNotAllowed;
    }
}
