<?php

namespace Ed\Http;

class Router {
    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @param Route $route
     * @return Router
     */
    public function addRoute(Route $route) {
        $this->routes[] = $route;
        return $this;
    }

    /**
     * @param string $path
     * @return Route|null
     */
    public function match($path) {
        /** @var Route $route */
        foreach ($this->routes as $route) {
            if ($route->match($path)) {
                return $route;
            }
        }
        return null;
    }
}
