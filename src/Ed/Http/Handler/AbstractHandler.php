<?php

namespace Ed\Http\Handler;

use Ed\Http\Exception\MethodNotAllowed;
use Ed\Http\Request;

abstract class AbstractHandler {
    /**
     * @param Request $request
     * @return mixed
     * @throws MethodNotAllowed
     */
    public function get(Request $request) {
        throw new MethodNotAllowed;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws MethodNotAllowed
     */
    public function post(Request $request) {
        throw new MethodNotAllowed;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws MethodNotAllowed
     */
    public function delete(Request $request) {
        throw new MethodNotAllowed;
    }
}
