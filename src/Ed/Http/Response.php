<?php

namespace Ed\Http;

use Ed\Setting;

class Response {
    /**
     * @var int
     */
    protected $code;

    /**
     * @var mixed
     */
    protected $data;

    /**
     * @param int $code
     * @param mixed $data
     */
    public function __construct($code, $data = null) {
        $this->code = $code;
        $this->data = $data;
    }

    public function send() {
        http_response_code($this->code);
        $data = [
            'code' => $this->code,
            'data' => $this->data,
        ];
        $json = json_encode($data) . PHP_EOL;
        header('Content-Type: application/json');
        echo $json;
        exit;
    }
}
