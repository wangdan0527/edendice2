<?php

namespace Ed\Http;

use Ed\Http\Exception\Unauthorized;
use Ed\Model\Session;
use Ed\Util;

class Request {
    /**
     * @var Route
     */
    protected $route;

    /**
     * @return string
     */
    public function getMethod() {
        return strtoupper(Util::akey($_SERVER, 'REQUEST_METHOD', 'GET'));
    }

    /**
     * @return string
     */
    public function getPath() {
        $uri = Util::akey($_SERVER, 'REQUEST_URI', '');
        if (!$uri) {
            return '';
        }
        return explode('?', $uri)[0];
    }

    /**
     * @return \stdClass
     */
    public function getJson() {
        $input = file_get_contents('php://input');
        $obj = json_decode($input);
        if (!is_object($obj)) {
            return new \stdClass;
        }
        return $obj;
    }

    /**
     * @return string
     */
    public function getRemoteAddr() {
        return Util::akey($_SERVER, 'REMOTE_ADDR', '');
    }

    /**
     * @return string
     */
    public function getUserAgent() {
        return Util::akey($_SERVER, 'HTTP_USER_AGENT', '');
    }

    /**
     * @param Route $route
     * @return Request
     */
    public function setRoute(Route $route) {
        $this->route = $route;
        return $this;
    }

    /**
     * @return Route
     */
    public function getRoute() {
        return $this->route;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getRouteVar($name) {
        return $this->route->getVar($name);
    }

    /**
     * @param string $name
     * @return string
     */
    public function getHeader($name) {
        $key = 'HTTP_' . strtoupper(str_replace('-', '_', $name));
        return Util::akey($_SERVER, $key, '');
    }

    /**
     * @return \Ed\Model\SessionBean|null
     */
    public function getSession() {
        $token = $this->getHeader('X-EdenDice-Session');
        if (!$token) {
            $token = $this->getQueryVar('session');
            if (!$token) {
                return null;
            }
        }
        return Session::findByToken($token);
    }

    /**
     * @return \Ed\Model\SessionBean|null
     * @throws Unauthorized
     */
    public function requireSession() {
        $session = $this->getSession();
        if (!$session) {
            throw new Unauthorized;
        }
        return $session;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getQueryVar($name) {
        return Util::akey($_GET, $name);
    }
}
