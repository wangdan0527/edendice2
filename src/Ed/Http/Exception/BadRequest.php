<?php

namespace Ed\Http\Exception;

class BadRequest extends AbstractException {
    /**
     * @var int
     */
    protected $code = 400;

    /**
     * @param array $errors
     * @throws BadRequest
     */
    static public function throwIf(array &$errors) {
        if ($errors) {
            throw new self($errors);
        }
    }

    /**
     * @param string $name
     * @param string $error
     * @throws BadRequest
     */
    static public function throwSingle($name, $error) {
        throw new self([$name => $error]);
    }
}
