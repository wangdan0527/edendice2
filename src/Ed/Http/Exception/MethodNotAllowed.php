<?php

namespace Ed\Http\Exception;

class MethodNotAllowed extends AbstractException {
    /**
     * @var int
     */
    protected $code = 405;
}
