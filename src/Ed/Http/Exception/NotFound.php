<?php

namespace Ed\Http\Exception;

class NotFound extends AbstractException {
    /**
     * @var int
     */
    protected $code = 404;
}
