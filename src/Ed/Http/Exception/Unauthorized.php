<?php

namespace Ed\Http\Exception;

class Unauthorized extends AbstractException {
    /**
     * @var int
     */
    protected $code = 401;
}
