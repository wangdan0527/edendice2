<?php

namespace Ed\Game;

use Ed\Http\Exception\BadRequest;
use Ed\Model\Account;
use Ed\Model\KeypairHash;

abstract class AbstractGame {
    /**
     * @param int $account
     * @param \stdClass $input
     */
    abstract public function play($account, \stdClass $input);

    /**
     * @param int $account
     * @return KeypairHash
     */
    protected function getNextHash($account) {
        $account = Account::findById($account);
        $user = $account->getUser();
        $keypair = $user->getKeypair();
        $seq = 0;
        $hash = $keypair->getNextHash($seq);
        return new KeypairHash($keypair->id, $hash, $seq);
    }

    /**
     * @param \stdClass $input
     * @return array
     * @throws BadRequest
     */
    abstract protected function validate(\stdClass $input);
}
