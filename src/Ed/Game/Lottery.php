<?php

namespace Ed\Game;

use Ed\Db\Db;
use Ed\Http\Exception\BadRequest;
use Ed\Setting;

class Lottery extends AbstractGame {
    /**
     * @param int $account
     * @param \stdClass $input
     * @return Result
     */
    public function play($account, \stdClass $input) {
        return Db::transaction(function() use($account, $input) {
            list($numbers) = $this->validate($input);
            $keypairHash = $this->getNextHash($account);

            $count = Setting::getInt('lottery_count');
            $max = Setting::getInt('lottery_max');

            mt_srand(hexdec(substr($keypairHash->getHash(), 0, 8)));
            $serverNumbers = [];
            for ($i = 0; $i < $count; $i ++) {
                do {
                    $rand = mt_rand(1, $max);
                } while (in_array($rand, $serverNumbers));
                $serverNumbers[] = $rand;
            }

            $match = array_values(array_intersect($serverNumbers, $numbers));

            sort($numbers);
            sort($serverNumbers);

            $payout = 0.0;
            if (count($match) == count($serverNumbers)) {
                $result = 'win';
                $resultDescription = 'JACKPOT!';
                $payout = 10000.0;
            } elseif (count($match) == 0) {
                $result = 'lose';
                $resultDescription = 'Lose';
            } else {
                $result = 'close';
                $resultDescription =
                    count($match) .
                    ' number' .
                    (count($match) == 1 ? '' : 's');
                switch (count($match)) {
                    case 1:
                        $payout = 1.0;
                        break;
                    case 2:
                        $payout = 2.0;
                        break;
                    case 3:
                        $payout = 4.0;
                        break;
                    case 4:
                        $payout = 10.0;
                        break;
                    case 5:
                        $payout = 50.0;
                        break;
                }
            }

            return new Result(
                $numbers,
                $serverNumbers,
                $result,
                $resultDescription,
                $payout,
                $keypairHash->getKeypair(),
                $keypairHash->getSeq()
            );
        });
    }

    /**
     * @param \stdClass $input
     * @return array
     * @throws BadRequest
     */
    protected function validate(\stdClass $input) {
        $count = Setting::getInt('lottery_count');
        $min = Setting::getInt('lottery_min');
        $max = Setting::getInt('lottery_max');

        $errors = [];
        $numbers = [];
        if (!isset($input->numbers) || !is_array($input->numbers)) {
            $errors['numbers'] = 'Please select your lottery numbers.';
        } else {
            $numbers = $input->numbers;
            if (count($input->numbers) != $count) {
                $errors['numbers'] = sprintf(
                    'Please select %d number(s).',
                    $count
                );
            } else {
                foreach ($numbers as $k => $v) {
                    $numbers[$k] = intval($v);
                    if ($numbers[$k] < $min || $numbers[$k] > $max) {
                        $errors['numbers'] = sprintf(
                            'Please select numbers between %d and %d.',
                            $min, $max
                        );
                    }
                }
            }
        }
        BadRequest::throwIf($errors);
        return [$numbers];
    }
}
