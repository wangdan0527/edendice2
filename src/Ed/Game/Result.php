<?php

namespace Ed\Game;

class Result {
    /**
     * @var mixed
     */
    protected $userPlay;

    /**
     * @var mixed
     */
    protected $serverPlay;

    /**
     * @var string
     */
    protected $result;

    /**
     * @var string
     */
    protected $resultDescription;

    /**
     * @var int
     */
    protected $bet;

    /**
     * @var float
     */
    protected $payout;

    /**
     * @var int
     */
    protected $keypair;

    /**
     * @var int
     */
    protected $seq;

    /**
     * @param mixed $userPlay
     * @param mixed $serverPlay
     * @param string $result
     * @param string $resultDescription
     * @param float $payout
     * @param int $keypair
     * @param int $seq
     */
    public function __construct($userPlay, $serverPlay, $result, $resultDescription, $payout, $keypair, $seq) {
        $this->userPlay = $userPlay;
        $this->serverPlay = $serverPlay;
        $this->result = $result;
        $this->resultDescription = $resultDescription;
        $this->payout = floatval($payout);
        $this->keypair = intval($keypair);
        $this->seq = intval($seq);
    }

    /**
     * @return mixed
     */
    public function getUserPlay() {
        return $this->userPlay;
    }

    /**
     * @return mixed
     */
    public function getServerPlay() {
        return $this->serverPlay;
    }

    /**
     * @return string
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * @return string
     */
    public function getResultDescription() {
        return $this->resultDescription;
    }

    /**
     * @return float
     */
    public function getPayout() {
        return $this->payout;
    }

    /**
     * @return int
     */
    public function getKeypair() {
        return $this->keypair;
    }

    /**
     * @return int
     */
    public function getSeq() {
        return $this->seq;
    }

    /**
     * @return array
     */
    public function serialize() {
        return [
            'user_play' => $this->userPlay,
            'server_play' => $this->serverPlay,
            'result' => $this->result,
            'result_description' => $this->resultDescription,
            'payout' => $this->payout,
            'seq' => $this->seq,
        ];
    }
}
