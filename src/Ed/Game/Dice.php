<?php

namespace Ed\Game;

use Ed\Db\Db;
use Ed\Http\Exception\BadRequest;
use Ed\Setting;
use Ed\Util;

class Dice extends AbstractGame {
    const MAX = 1000000;

    /**
     * @param int $account
     * @param \stdClass $input
     * @return Result
     */
    public function play($account, \stdClass $input) {
        return Db::transaction(function() use($account, $input) {
            list($chances, $side) = $this->validate($input);
            $keypairHash = $this->getNextHash($account);

            $margin = Setting::getFloat('dice_margin');
            $multiplier = ((1 - $margin) * 100) / $chances;
            $userPlay = $this->getUserPlay($keypairHash->getHash());
            $serverPlay = $this->getServerPlay($multiplier, $margin);
            if ($side == 'high') {
                $result = $userPlay >= $serverPlay ? 'win' : 'lose';
                $payout = $userPlay >= $serverPlay ? $multiplier - 1.0 : 0.0;
            } else {
                $serverPlay = self::MAX - $serverPlay;
                $result = $userPlay <= $serverPlay ? 'win' : 'lose';
                $payout = $userPlay <= $serverPlay ? $multiplier - 1.0 : 0.0;
            }

            return new Result(
                sprintf('%0.4f', $userPlay / self::MAX * 100),
                sprintf('%0.4f', $serverPlay / self::MAX * 100),
                $result,
                ucfirst($result),
                $payout,
                $keypairHash->getKeypair(),
                $keypairHash->getSeq()
            );
        });
    }

    /**
     * @param \stdClass $input
     * @return array
     * @throws BadRequest
     */
    protected function validate(\stdClass $input) {
        $errors = [];
        $chances = intval(Util::okey($input, 'chances'));
        if ($chances < 5 || $chances > 95) {
            $errors['chances'] = 'Please select a value between 5% and 95%.';
        }
        $side = Util::okey($input, 'side');
        if (!$side || !in_array($side, ['low', 'high'])) {
            $errors['side'] = 'Please choose to bet high or low.';
        }
        BadRequest::throwIf($errors);
        return [$chances, $side];
    }

    /**
     * @param string $hash
     * @param int $max
     * @return int
     */
    protected function getUserPlay($hash) {
        return intval(ceil(
            (hexdec(substr($hash, 0, 6)) + 1) /
            pow(16, 6) *
            self::MAX
        ));
    }

    /**
     * @param float $multiplier
     * @param float $margin
     * @param int $max
     * @return int
     */
    protected function getServerPlay($multiplier, $margin) {
        $odds = (1 - $margin) / $multiplier;
        return intval(ceil(self::MAX - (self::MAX * $odds)));
    }
}
