<?php

namespace Ed\Game;

use Ed\Db\Db;
use Ed\Http\Exception\BadRequest;

class EdenDice extends AbstractGame {
    /**
     * @var array
     */
    static protected $payouts = [
        '4.2' => 0.8,
        '4.4' => 0.7,
        '4.6' => 0.6,
        '4.8' => 0.5,
    ];

    /**
     * @param int $account
     * @param \stdClass $input
     * @return Result
     */
    public function play($account, \stdClass $input) {
        return Db::transaction(function() use($account, $input) {
            list($roll, $payout) = $this->validate($input);
            $keypairHash = $this->getNextHash($account);

            if (!$roll) {
                $roll = ceil(
                    (hexdec(substr($keypairHash->getHash(), 4, 4)) + 1) /
                    65536 *
                    6
                );
            }
            $serverRoll = ceil(
                (hexdec(substr($keypairHash->getHash(), 0, 4)) + 1) /
                65536 *
                6
            );

            $diff = abs($roll - $serverRoll);
            if ($diff == 0) {
                $result = 'win';
            } elseif ($diff == 1 || $diff == 5) {
                $result = 'close';
            } else {
                $result = 'lose';
            }

            if ($result == 'win') {
                $payout = floatval($payout);
            } elseif ($result == 'close') {
                $payout = self::$payouts[$payout];
            } else {
                $payout = 0.0;
            }

            return new Result(
                $roll,
                $serverRoll,
                $result,
                ucfirst($result),
                $payout,
                $keypairHash->getKeypair(),
                $keypairHash->getSeq()
            );
        });
    }

    /**
     * @param \stdClass $input
     * @return array
     * @throws BadRequest
     */
    protected function validate(\stdClass $input) {
        $errors = [];
        $roll = 0;
        if (!isset($input->roll)) {
            $errors['roll'] = 'Please select a roll.';
        } else {
            if (is_numeric($input->roll)) {
                $roll = intval($input->roll);
                if ($roll < 1 || $roll > 6) {
                    $errors['roll'] = 'Please select a number between 1 and 6.';
                }
            } elseif ($input->roll !== 'random') {
                $errors['roll'] = 'Please select a number between 1 and 6, or choose random.';
            }
        }
        $payout = null;
        if (!isset($input->payout)) {
            $errors['payout'] = 'Please select a payout.';
        } else {
            $payout = $input->payout;
            if (!array_key_exists($input->payout, self::$payouts)) {
                $errors['payout'] = 'Please select a valid payout.';
            }
        }
        BadRequest::throwIf($errors);
        return [$roll, $payout];
    }
}
