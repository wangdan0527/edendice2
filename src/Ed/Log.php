<?php

namespace Ed;

class Log {
    /**
     * @var resource
     */
    static protected $handle;

    /**
     * @return resource
     */
    static protected function getHandle() {
        if (!self::$handle) {
            $file = Setting::get('log_file');
            self::$handle = @fopen($file, 'a');
        }
        return self::$handle;
    }

    /**
     * @param string $level
     * @param string $message
     */
    static protected function write($level, $message) {
        if ($handle = self::getHandle()) {
            $line =
                date('[Y-m-d H:i:s]') .
                ' ' .
                posix_getpid() .
                ' ' .
                $level .
                ' - ' .
                $message .
                PHP_EOL;
            fwrite(self::getHandle(), $line);
        }
    }

    static public function info() {
        $message = call_user_func_array('sprintf', func_get_args());
        self::write('INFO', $message);
    }

    static public function error() {
        $message = call_user_func_array('sprintf', func_get_args());
        self::write('ERROR', $message);
    }
}
