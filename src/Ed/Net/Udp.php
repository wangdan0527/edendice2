<?php

namespace Ed\Net;

use Ed\Setting;

class Udp {
    /**
     * @param string $type
     * @param mixed $data
     * @param array $to
     */
    static public function broadcast($type, $data, $to = []) {
        $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        socket_set_option($sock, SOL_SOCKET, SO_BROADCAST, 1);
        $payload = json_encode([
            'type' => $type,
            'data' => $data,
            'to' => $to,
        ]);
        socket_sendto(
            $sock,
            $payload,
            strlen($payload),
            0,
            '255.255.255.255',
            Setting::getInt('udp_port')
        );
        socket_close($sock);
    }
}
