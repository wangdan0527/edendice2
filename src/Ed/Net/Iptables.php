<?php

namespace Ed\Net;

use Ed\Log;

class Iptables {
    /**
     * @param array $ips
     */
    static public function setBannedIps(array $ips) {
        $bannedIps = self::getBannedIps();

        // remove rules not in $ips
        $diff = array_diff($bannedIps, $ips);
        foreach ($diff as $ip) {
            Log::info('Unbanning ' . $ip . '.');
            exec('iptables -D INPUT -s ' . $ip . ' -j DROP');
        }

        // add rules not in $bannedIps
        $diff = array_diff($ips, $bannedIps);
        foreach ($diff as $ip) {
            Log::info('Banning ' . $ip . '.');
            exec('iptables -A INPUT -s ' . $ip . ' -j DROP');
        }
    }

    /**
     * @return array
     */
    static public function getBannedIps() {
        $output = [];
        exec('iptables -nL | grep DROP | awk \'{print $4}\'', $output);
        return array_filter(array_map('trim', $output));
    }
}
