SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS users;
CREATE TABLE users (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(16) NOT NULL,
    email VARCHAR(255) DEFAULT NULL,
    password CHAR(60) DEFAULT NULL,
    keypair_id INT UNSIGNED,
    mfa TINYINT(1) NOT NULL DEFAULT 0,
    secret CHAR(16) NOT NULL,
    affiliate_code CHAR(16) NOT NULL,
    affiliate_user_id INT UNSIGNED DEFAULT NULL,
    status ENUM('active', 'deleted') NOT NULL DEFAULT 'active',
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED,
    UNIQUE KEY(username),
    UNIQUE KEY(email),
    UNIQUE KEY(secret),
    UNIQUE KEY(affiliate_code)
);

DROP TABLE IF EXISTS sessions;
CREATE TABLE sessions (
    token CHAR(32) NOT NULL,
    user_id INT UNSIGNED NOT NULL,
    ip VARCHAR(255) NOT NULL,
    user_agent VARCHAR(255) NOT NULL,
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED,
    PRIMARY KEY(token, user_id)
);

DROP TABLE IF EXISTS currencies;
CREATE TABLE currencies (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    code CHAR(3) NOT NULL,
    scheme VARCHAR(255) NOT NULL,
    required_confirmations INT UNSIGNED NOT NULL,
    setting_prefix VARCHAR(255) NOT NULL,
    balance INT UNSIGNED NOT NULL DEFAULT 0,
    profit INT UNSIGNED NOT NULL DEFAULT 0,
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED,
    UNIQUE KEY(name),
    UNIQUE KEY(scheme)
);

DROP TABLE IF EXISTS games;
CREATE TABLE games (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    instructions TEXT,
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED,
    UNIQUE KEY(name)
);

DROP TABLE IF EXISTS plays;
CREATE TABLE plays (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    account_id INT UNSIGNED NOT NULL,
    game_id INT UNSIGNED NOT NULL,
    keypair_id INT UNSIGNED NOT NULL,
    keypair_seq INT UNSIGNED NOT NULL,
    user_play TEXT NOT NULL,
    server_play TEXT NOT NULL,
    result ENUM('win', 'close', 'lose') NOT NULL,
    result_description VARCHAR(255) DEFAULT NULL,
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED
);

DROP TABLE IF EXISTS accounts;
CREATE TABLE accounts (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id INT UNSIGNED NOT NULL,
    currency_id INT UNSIGNED NOT NULL,
    balance INT NOT NULL,
    investment INT NOT NULL,
    investment_weighting DOUBLE NOT NULL DEFAULT 0,
    address VARCHAR(35) NOT NULL,
    emergency_address VARCHAR(35),
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED
);

DROP TABLE IF EXISTS account_addresses;
CREATE TABLE account_addresses (
    account_id INT UNSIGNED NOT NULL,
    address VARCHAR(35) NOT NULL,
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED,
    PRIMARY KEY(account_id, address)
);

DROP TABLE IF EXISTS transactions;
CREATE TABLE transactions (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    account_id INT UNSIGNED NOT NULL,
    type ENUM('deposit', 'withdrawal', 'win', 'bet', 'adjustment', 'investment', 'divestment', 'affiliate') NOT NULL,
    txid CHAR(64),
    amount INT NOT NULL,
    description VARCHAR(255) NOT NULL,
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED
);

DROP TABLE IF EXISTS contacts;
CREATE TABLE contacts (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    phone VARCHAR(255),
    message TEXT NOT NULL,
    ip VARCHAR(255) NOT NULL,
    user_agent VARCHAR(255) NOT NULL,
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED
);

DROP TABLE IF EXISTS settings;
CREATE TABLE settings (
    name VARCHAR(255) NOT NULL PRIMARY KEY,
    value TEXT NOT NULL,
    readonly TINYINT(1) NOT NULL DEFAULT 0,
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED
);

DROP TABLE IF EXISTS keypairs;
CREATE TABLE keypairs (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_key CHAR(40) NOT NULL,
    server_key CHAR(40) NOT NULL,
    seq INT UNSIGNED NOT NULL,
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED,
    UNIQUE KEY(user_key, server_key)
);

DROP TABLE IF EXISTS currency_transactions;
CREATE TABLE currency_transactions (
    currency_id INT UNSIGNED NOT NULL,
    txid CHAR(64) NOT NULL,
    address VARCHAR(35) NOT NULL,
    category ENUM('send', 'receive', 'generate', 'immature', 'orphan') NOT NULL,
    amount INT NOT NULL,
    confirmations INT UNSIGNED NOT NULL,
    ts INT UNSIGNED NOT NULL,
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED,
    PRIMARY KEY(currency_id, txid)
);

DROP TABLE IF EXISTS withdrawals;
CREATE TABLE withdrawals (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    account_id INT UNSIGNED NOT NULL,
    amount INT NOT NULL,
    address VARCHAR(255) NOT NULL,
    status ENUM('new', 'paid', 'rejected', 'error') NOT NULL,
    ip VARCHAR(255) NOT NULL,
    user_agent VARCHAR(255) NOT NULL,
    created INT UNSIGNED NOT NULL,
    modified INT UNSIGNED
);

SET FOREIGN_KEY_CHECKS = 1;

ALTER TABLE sessions ADD FOREIGN KEY fk_sessions_user_id (user_id) REFERENCES users (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE plays ADD FOREIGN KEY fk_plays_account_id (account_id) REFERENCES accounts (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE plays ADD FOREIGN KEY fk_plays_game_id (game_id) REFERENCES games (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE plays ADD FOREIGN KEY fk_plays_keypair_id (keypair_id) REFERENCES keypairs (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE accounts ADD FOREIGN KEY fk_accounts_user_id (user_id) REFERENCES users (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE accounts ADD FOREIGN KEY fk_plays_currency_id (currency_id) REFERENCES currencies (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE account_addresses ADD FOREIGN KEY fk_account_addresses_account_id (account_id) REFERENCES accounts (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE transactions ADD FOREIGN KEY fk_transactions_account_id (account_id) REFERENCES accounts (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE currency_transactions ADD FOREIGN KEY fk_currency_transactions (currency_id) REFERENCES currencies (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE users ADD FOREIGN KEY fk_users_keypair_id (keypair_id) REFERENCES keypairs (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE users ADD FOREIGN KEY fk_affiliate_user_id (affiliate_user_id) REFERENCES users (id) ON UPDATE CASCADE ON DELETE RESTRICT;

INSERT INTO settings (name, value, created) VALUES ('bitcoin_url', 'http://bitcoinrpc:21CuDFRPVaaqGLUjKPhZkgxsj4FwUD6RFMfsfSj11a1H@130.211.101.230:8332', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('bitcoin_withdrawal_sms_amount', '100', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('litecoin_url', 'http://litecoinrpc:CavgxFZLGZjmTJvUwMMAoNYoTqZENhDqjYBwN72boTpR@130.211.101.230:9332', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('litecoin_withdrawal_sms_amount', '1000', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('api_url', 'http://api.dev.edendice.com/', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('www_url', 'http://dev.edendice.com/', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('exchange_url', 'http://exchange.edendice.com:8080/', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('dice_margin', '0.01', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('lottery_min', '1', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('lottery_max', '40', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('lottery_count', '6', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('log_file', '/usr/local/edendice/edendice.log', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('udp_port', '19817', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('captcha_public_key', '6LdVp_wSAAAAAHoOGuPZiAGmGYWuEZZOgfaYwfA-', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('captcha_secret_key', '6LdVp_wSAAAAAHNO0VrovOPojigCQjgxwyNkhy2I', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('banned_ips', '', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('clickatell_username', 'UncleCarbs2', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('clickatell_password', 'ASIdbaTUdEYSRK', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('clickatell_api_id', '3551563', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('admin_sms', '+27798855137', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('divest_commission', '0.07', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, readonly, created) VALUES ('investment_payout_time', '', 1, UNIX_TIMESTAMP());
INSERT INTO settings (name, value, readonly, created) VALUES ('affiliate_payout_time', '', 1, UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('investment_payout', '0.03', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('bitcoin_tx_fee', '10000', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('litecoin_tx_fee', '100000', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('memcached_servers', '127.0.0.1:11211', UNIX_TIMESTAMP());
INSERT INTO settings (name, value, created) VALUES ('max_payout_ratio', '0.005', UNIX_TIMESTAMP());
INSERT INTO users (id, username, password, secret, affiliate_code, created) VALUES (1, 'NeilG', '$2y$12$INdQNvlN6RimOfdWsrmbgu4FrWSj1yB2peCcTu5h0eCA0zbNP7SXW', 'AAAAAAAAAAAAAAAA', 'BBBBBBBBBBBBBBBB', UNIX_TIMESTAMP());
INSERT INTO currencies (id, name, code, scheme, required_confirmations, setting_prefix, created) VALUES (1, 'Bitcoin', 'BTC', 'bitcoin', 6, 'bitcoin', UNIX_TIMESTAMP());
INSERT INTO currencies (id, name, code, scheme, required_confirmations, setting_prefix, created) VALUES (2, 'Litecoin', 'LTC', 'litecoin', 1, 'litecoin', UNIX_TIMESTAMP());
INSERT INTO accounts (user_id, currency_id, balance, investment, address, created) VALUES (1, 1, 0, 0, '1PLqBTKSsejPMiinLNcMAUJ2gsguZFikKc', UNIX_TIMESTAMP());
INSERT INTO account_addresses (account_id, address, created) VALUES (1, '1PLqBTKSsejPMiinLNcMAUJ2gsguZFikKc', UNIX_TIMESTAMP());
INSERT INTO games (id, name, instructions, created) VALUES (1, 'Dice', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p><p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>', UNIX_TIMESTAMP());
INSERT INTO games (id, name, instructions, created) VALUES (2, 'Eden Dice', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p><p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>', UNIX_TIMESTAMP());
INSERT INTO games (id, name, instructions, created) VALUES (3, 'Lottery', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p><p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>', UNIX_TIMESTAMP());
