<?php

date_default_timezone_set('UTC');

spl_autoload_register(function($class) {
    $classParts = explode('\\', trim($class, '\\'));
    if ($classParts[0] == 'Ed') {
        $classFile = __DIR__ . '/src/' . join('/', $classParts) . '.php';
        require_once $classFile;
    }
});

\Ed\Db\Db::setPdo(new PDO('mysql:host=localhost;dbname=edendice', 'root', ''));
\Ed\Setting::load();

require_once __DIR__ . '/vendor/autoload.php';
